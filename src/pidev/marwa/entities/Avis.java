/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.entities;

/**
 *
 * @author Touha
 */
public class Avis {
    public int id;
    public int rating;
    public String commentaire;
    public int idproduit;
    public int user;

    public Avis(int rating, String commentaire, int idproduit, int user) {
        this.rating = rating;
        this.commentaire = commentaire;
        this.idproduit = idproduit;
        this.user = user;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public Avis(int rating, String commentaire, int idproduit) {
        this.rating = rating;
        this.commentaire = commentaire;
        this.idproduit = idproduit;
    }

    public int getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(int idproduit) {
        this.idproduit = idproduit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public String toString() {
        return "Avis{" + "id=" + id + ", rating=" + rating + ", commentaire=" + commentaire + '}';
    }

    public Avis() {
    }

    public Avis(int rating, String commentaire) {
        this.rating = rating;
        this.commentaire = commentaire;
    }

    public Avis(int id, int rating, String commentaire) {
        this.id = id;
        this.rating = rating;
        this.commentaire = commentaire;
    }
    
    
    
    
}
