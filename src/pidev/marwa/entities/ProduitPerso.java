/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.entities;

/**
 *
 * @author gaalo
 */
public class ProduitPerso {
    

 private int id ;
private String nom; 

private String image_name; 

private String updated_at ;

private double prix; 

private String description ;

private int quantité;

    public ProduitPerso() {
    }

    public ProduitPerso(int id, String nom, String image_name, double prix, String description, int quantité) {
        this.id = id;
        this.nom = nom;
        this.image_name = image_name;
        this.prix = prix;
        this.description = description;
        this.quantité = quantité;
    }

    public ProduitPerso( String nom, String image_name, double prix, String description) {
     
        this.nom = nom;
        this.image_name = image_name;
        this.prix = prix;
        this.description = description;
    }

    public ProduitPerso(int id, String nom, String image_name, String updated_at, double prix, String description, int quantité) {
        this.id = id;
        this.nom = nom;
        this.image_name = image_name;
        this.updated_at = updated_at;
        this.prix = prix;
        this.description = description;
        this.quantité = quantité;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantité() {
        return quantité;
    }

    public void setQuantité(int quantité) {
        this.quantité = quantité;
    }

    @Override
    public String toString() {
        return "ProduitPerso{" + "id=" + id + ", nom=" + nom + ", image_name=" + image_name + ", updated_at=" + updated_at + ", prix=" + prix + ", description=" + description + ", quantit\u00e9=" + quantité + '}';
    }





   
    
}
