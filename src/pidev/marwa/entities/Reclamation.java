/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.entities;

/**
 *
 * @author gaalo
 */
public class Reclamation {

    private int id, idUser, idproduit;
    private String specification;
    private String date;

    public Reclamation() {
    }

    public Reclamation(String specification) {
        this.specification = specification;
    }

    public Reclamation(String specification, String date) {
        this.specification = specification;
        this.date = date;
    }

    public Reclamation(int id, int idproduit, int idUser, String date, String specification) {
        this.id = id;
        this.idproduit = idproduit;

        this.idUser = idUser;
        this.date = date;

        this.specification = specification;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(int idproduit) {
        this.idproduit = idproduit;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Reclamation{" + "id=" + id + ", idUser=" + idUser + ", idproduit=" + idproduit + ", specification=" + specification + ", date=" + date + '}';
    }

}
