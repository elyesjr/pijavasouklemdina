/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import static pidev.marwa.Presentation.ListeproduitpController.idpro;
import pidev.marwa.entities.ProduitPerso;
import pidev.marwa.services.CrudProduitPerso;
import pidev.marwa.util.DataSource;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class ModifierproduitpController implements Initializable {

    @FXML
    private Button imagebtn;
    @FXML
    private Label nomimage;
    @FXML
    private ImageView img;
    @FXML
    private JFXSlider prix;
    @FXML
    private JFXTextArea description;
    @FXML
    private JFXButton btnmod;
    @FXML
    private JFXTextField nom;
     private ObservableList<ProduitPerso> data;
    private Connection mycon;
     public File fileSelected = null;
    public String path;
    @FXML
    private AnchorPane rootpanne;
    @FXML
    private Button btnretour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          LoadData();
         
    }    
private void LoadData() {
        Statement ste;
    Connection cn = DataSource.getInstance().getConnection();
    PreparedStatement pst;
    
        ProduitPerso e = new ProduitPerso();
        try {
            System.out.println("" + idpro);

            ResultSet res = cn.createStatement().executeQuery("SELECT * FROM `produitp` where id = " + idpro + " ");
            while (res.next()) {
                
            
            ProduitPerso p = new ProduitPerso(res.getString(2), res.getString(3), idpro, res.getString(6));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeproduitpController.class.getName()).log(Level.SEVERE, null, ex);

        }

        nom.setText(e.getNom());
        description.setText(e.getDescription());
        
        prix.setValue(e.getPrix());
        if (fileSelected != null) {
            path = fileSelected.getAbsolutePath();

            path.replace("\\", "\\\\");
            System.out.println("path" + path);
        }
        e.setImage_name(path);
}

    @FXML
    private void imagechoice(ActionEvent event) throws IOException {
         FileChooser fc = new FileChooser();
        fileSelected = fc.showOpenDialog(null);

        System.out.println("" + fileSelected.getCanonicalPath());
        nomimage.setText(fileSelected.getName());
        File file = new File(fileSelected.getAbsolutePath());

        Image image = new Image(file.toURI().toString());
    }

    

 


    

    

    @FXML
    private void update(ActionEvent event) throws SQLException, IOException{

        if (nom.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Remplir les champs", "Remplir les champs", 2);
        } else {

            ProduitPerso r= new ProduitPerso(nom.getText(), nomimage.getText(), prix.getValue(), description.getText());
            CrudProduitPerso c = new CrudProduitPerso();
            c.UpdateProduitPerso(r, idpro);
            JOptionPane.showMessageDialog(null, "Modification avec succes", "Modification avec succes", 1);
    }

}

    @FXML
    private void ajouter(MouseEvent event) {
    }

    @FXML
    private void premierPage(ActionEvent event) throws IOException {
         AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }
}
    
