/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.entities.user;
import pidev.marwa.services.CrudReclamation;
import pidev.services.UserService;


/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class AjoutRecController implements Initializable {

    @FXML
    private DatePicker date;
    @FXML
    private TextArea specification;
    @FXML
    private Button ajouter;
    public static String mail;
    @FXML
    private AnchorPane rootpanne;
    @FXML
    private Button btnretour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void ajouter(ActionEvent event) throws SQLException {
        String DATE_PATTERN = "dd/MM/yyyy";

        DateTimeFormatter DATE_FORMATTER
                = DateTimeFormatter.ofPattern(DATE_PATTERN);
        String date = DATE_FORMATTER.format(this.date.getValue());
        CrudReclamation s = new CrudReclamation();
        Reclamation e = new Reclamation();
        e.setSpecification(specification.getText());
        e.setDate(date);

        e.setIdUser(8);

        System.out.println(specification.getText());
        System.out.println(date);

        if (specification.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "veuillez remplir tous les champs oubien vérifiez Votre adresse Email", "veuillez remplir tous les champs oubien vérifiez Votre adresse Email", 2);
 } 
      
         else {
            s.ajouterReclamation(e);
//            user u = new user();
//            UserService ser = new UserService();
//           ser.recupEmail(user.id);
//            sendMail.sending(/*u.getEmail()*/"marwa.gaaloul@esprit.tn","mokhtarammar.ma@gmail.com","Demande d'inscription ",
//         "votre demande a été traitée");
//            JOptionPane.showMessageDialog(null, "Votre demande va etre traitée", "Votre demande va etre traitée", 2);

        }
    }

    @FXML
    private void premierPage(ActionEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

}
