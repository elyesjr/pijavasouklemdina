/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import pidevjava.SendSms;
import pidev.marwa.entities.ProduitPerso;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.entities.user;
import pidev.marwa.services.CrudProduitPerso;
import pidev.marwa.services.CrudReclamation;
import pidev.marwa.services.UserService;

/**
 * FXML Controller class
 *
 * @author
 */
public class AjoutProduitPController implements Initializable {

    @FXML
    private Button imagebtn;
    @FXML
    private ImageView img;
    @FXML
    private JFXSlider prix;
    @FXML
    private JFXTextArea description;
    @FXML
    private JFXButton commander;
    @FXML
    private JFXTextField nom;
    @FXML
    private Label nomimage;

    public File fileSelected = null;
    public String path;
    public static int randomNum;
    @FXML
    private AnchorPane rootpanne;
    @FXML
    private Button btnretour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
@FXML
    private void ajouter(ActionEvent event) {
        CrudProduitPerso s = new CrudProduitPerso();
        ProduitPerso e = new ProduitPerso();
       UserService u =new UserService();
       user k=new user();
        e.setNom(nom.getText());
        e.setPrix((int) prix.getValue());
        e.setDescription(description.getText());
        if (fileSelected != null) {
            path = fileSelected.getAbsolutePath();

            path.replace("\\", "\\\\");
            System.out.println("path" + path);
        }
        e.setImage_name(path);
        s.ajouterProduitPerso(e);
//        SendSms sms = new SendSms();
//        
//        randomNum = ThreadLocalRandom.current().nextInt(1000,9000);
//        System.out.println("random"+randomNum);
//        int nmtel = 28425930;
//        sms.sendSms(randomNum,nmtel);
        JOptionPane.showMessageDialog(null,"votre demande va étre traitée",""
                + "votre demande va étre traité",2);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("VerificationFXML.fxml"));
                      int input = JOptionPane.showConfirmDialog(null, " votre demmande va étre traiteé");

    }

    @FXML
    public void imagechoice(ActionEvent event) throws IOException {

        FileChooser fc = new FileChooser();
        fileSelected = fc.showOpenDialog(null);

        System.out.println("" + fileSelected.getCanonicalPath());
        nomimage.setText(fileSelected.getName());
        File file = new File(fileSelected.getAbsolutePath());

        Image image = new Image(file.toURI().toString());

        // imageview.setImage(image);
    }

//        
//        if (nom.getValue().toString().equals("") || speci.getText().equals("")
//               ) {
//            JOptionPane.showMessageDialog(null, "veuillez remplir tous les champs oubien vérifiez Votre adresse Email", "veuillez remplir tous les champs oubien vérifiez Votre adresse Email", 2);
//
//        } else {
//            s.ajouterProduitPerso(e);
//            JOptionPane.showMessageDialog(null, "Votre demande va etre traitée", "Votre demande va etre traitée", 2);
//
//        }

   

    @FXML
    private void premierPage(ActionEvent event) throws IOException {
         AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }
}
