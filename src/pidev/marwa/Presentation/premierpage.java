/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import static pidev.marwa.Presentation.ListeproduitpController.idpro;
import pidev.marwa.entities.ProduitPerso;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.services.CrudProduitPerso;
import pidev.marwa.services.CrudReclamation;
import pidev.marwa.util.DataSource;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class premierpage implements Initializable {

    @FXML
    private TableView<ProduitPerso> table;
    @FXML
    private TableColumn<ProduitPerso, String> nom;
    @FXML
    private TableColumn<ProduitPerso, String> image;
    @FXML
    private TableColumn<ProduitPerso, String> prix;
    @FXML
    private TableColumn<ProduitPerso, String> description;
    private ObservableList<ProduitPerso> data;
    private Connection mycon;
    public static int  id;
    @FXML
    private TableColumn<ProduitPerso, String> idl;
    @FXML
    private TableColumn<ProduitPerso, String> updated_at;
    @FXML
    private TableColumn<ProduitPerso, String> quantite;
    @FXML
    private Button details;
    @FXML
    private Label nomd;
    @FXML
    private Label descriptiond;
    @FXML
    private ImageView imagedd;
    @FXML
    private JFXButton btnretour;
    @FXML
    private AnchorPane rootpanne;
    @FXML
    private JFXButton btnSupprimer;
    @FXML
    private JFXButton personnaliser;
    @FXML
    private JFXButton reclamation;
    static MediaPlayer mediaplayermp3;
    @FXML
    private Button stopmusic;
    @FXML
    private Button playmusic;
    @FXML
    private Label meiilleur;
   
    
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        LoadData();
table.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
            try {
                showDetails(newValue);
            } catch (SQLException ex) {
                Logger.getLogger(ListeproduitpController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        // TODO
    }

    private void LoadData() {
        CrudProduitPerso CBS = new CrudProduitPerso();
        data = FXCollections.observableArrayList();
        List<ProduitPerso> list = new ArrayList<>();

        try {
            list = CBS.DisplayAllProduitPerso();
        } catch (SQLException ex) {
            Logger.getLogger(premierpage.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (ProduitPerso b : list) {
            data.add(b);
        }
        idl.setCellValueFactory(new PropertyValueFactory<>("id"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        image.setCellValueFactory(new PropertyValueFactory<>("image_name"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        updated_at.setCellValueFactory(new PropertyValueFactory<>("updated_at"));
        quantite.setCellValueFactory(new PropertyValueFactory<>("quantite"
                + ".0..t.3e"));

        table.setItems(null);
        table.setItems(data);
    }
    private void showDetails(ProduitPerso ProduitPerso) throws SQLException {
        if (ProduitPerso == null) {
            
            
         //   System.out.println("" + ProduitPerso.getNom());

            nomd.setText("");
            descriptiond.setText("");
        } else {
            
          //  System.out.println("" + ProduitPerso.getNom());
            nomd.setText(ProduitPerso.getNom());
            descriptiond.setText(ProduitPerso.getDescription());
            File file = new File(ProduitPerso.getImage_name());

            Image image = new Image(file.toURI().toString());

            imagedd.setImage(image);

        }
    }
 @FXML
    public void afficherDetails(ActionEvent event) throws IOException {
           
        ObservableList<ProduitPerso> selectedRows, all;
        all = table.getItems();

        
        selectedRows = table.getSelectionModel().getSelectedItems();

        
        for (ProduitPerso b : selectedRows) {
         
                idpro = b.getId();
                nomd.setText(b.getNom());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("afficherdetail.fxml"));
                Parent root=fxmlLoader.load();
                AfficherdetailController aff=fxmlLoader.getController();
                aff.setDescription(b.getDescription());
                aff.setNompp(b.getNom());
                aff.setPrix(b.getPrix());
                aff.setQuantite(b.getQuantité());
                  File file = new File(b.getImage_name());

            Image image = new Image(file.toURI().toString());

                aff.imagedd.setImage(imagedd.getImage());
//                Scene scene = new Scene(fxmlLoader.load());
//                Stage stage = new Stage();
//
//                stage.setScene(scene);
//                stage.show();
table.getScene().setRoot(root);
               
          

        }
    }

    
    @FXML
    private void premierPage (ActionEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

    
@FXML
    private void personnaliser (ActionEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("AjoutProduitP.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

    @FXML
    private void deleteact(ActionEvent event) {
        try {
          //  System.out.println("marwa");
            ProduitPerso produiterso = table.getSelectionModel().getSelectedItem();
            
            
            
             int input = JOptionPane.showConfirmDialog(null, "voulez vous confirmer suppression ?");
        if (input == 0) {
 CrudProduitPerso.getInstance().deleteRaiting(produiterso.getId());
  CrudProduitPerso.getInstance().deleteReclamation(produiterso.getId());
           CrudProduitPerso.getInstance().DeleteProduitPerso(produiterso.getId());



        }
           LoadData();

//            List<ProduitPerso> produitpersos = CrudProduitPerso.getInstance().DisplayAllProduitPerso();
//
//            ObservableList<ProduitPerso> data = FXCollections.observableArrayList(produitpersos);
//            table.setItems(data);
        } catch (SQLException ex) {
            Logger.getLogger(ListeproduitpController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void reclamation(ActionEvent event)throws IOException {
         AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("ajoutRec.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }
    @FXML
    private void playmusic(ActionEvent event)  {
        try {

            Media musicfile = new Media("file:///C://Users//gaalo//OneDrive//Documents//NetBeansProjects//soukelmedina//src//soukelmedina//marwa//soukelmedina.mp3");
            mediaplayermp3 = new MediaPlayer(musicfile);
            
             mediaplayermp3.play();
             

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    @FXML
    private void pausevedio(ActionEvent event) {
         mediaplayermp3.stop();
    }
//        private void LoadDataBest() throws SQLException {
//        Calendar today = Calendar.getInstance();
//       Statement ste;
//    Connection cn = DataSource.getInstance().getConnection();
//    PreparedStatement pst;
//
//        data = FXCollections.observableArrayList();
//        try {
//            ResultSet rs = cn.createStatement().executeQuery("SELECT * FROM `rating2` WHERE enabled=1 order by idproduitp DESC");
//            if (rs.next()) {
//                System.out.println("today.getDay()" + today.getTime().getDate());
//                if (today.getTime().getDate() == 1 ) //notified zayda , rs.getint(17) zayda 
//                {
//                    System.out.println("idbest" + rs.getInt(1));
//                    String req = " UPDATE `etablisment` SET `notified` = '1'";
//                    PreparedStatement statement = cnx.getConnection().prepareStatement(req);
//                    statement.executeUpdate();
//                    System.out.println("rs.getString(8)" + rs.getString(8));
//                    //SendMail.sending(rs.getString(8), "AllForKids", "Félicitation", "Félicitation vous étes le meilleur établissement du cet mois");
//
//                } else if (today.getTime().getDate() >= 1) {
//                    String req = " UPDATE `allforkids`.`etablisment` SET `notified` = '0'";
//                    PreparedStatement statement = cnx.getConnection().prepareStatement(req);
//                    statement.executeUpdate();
//
//                    ResultSet rs2 = cn.createStatement().executeQuery("SELECT * FROM `etablisment` WHERE enabled=1 order by nbrparticipants DESC");
//                    
//                    if (rs2.next()) {
//                        
//                            meilleur.setText(rs2.getString(2));
//                           
//                            
//                        
//                    }
//                }
//
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(premierPage(event).class.getName()).log(Level.SEVERE, null, ex);
//
//        }
//
//    }

}
