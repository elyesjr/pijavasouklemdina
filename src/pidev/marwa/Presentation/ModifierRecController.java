/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.services.CrudReclamation;
import static pidev.marwa.Presentation.ListeproduitpController.id;

public class ModifierRecController implements Initializable {

    @FXML
    private TextArea specification;
    @FXML
    private Button modifier;
    @FXML
    private DatePicker date;

    @FXML
    private AnchorPane AnchorPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        LoadData();
        // TODO

    }

    private void LoadData() {
        String DATE_PATTERN = "dd/MM/yyyy";

        DateTimeFormatter DATE_FORMATTER
                = DateTimeFormatter.ofPattern(DATE_PATTERN);
        String date = DATE_FORMATTER.format(this.date.getValue());
        CrudReclamation crs = new CrudReclamation();
        Reclamation r = new Reclamation();

        try {
            r = (Reclamation) crs.DisplayAllReclamation();
        } catch (SQLException ex) {
            Logger.getLogger(ListeReclamationController.class.getName()).log(Level.SEVERE, null, ex);
        }

        specification.setText(r.getSpecification());

    }

    @FXML

    private void modifier(ActionEvent event) throws SQLException, IOException {

        if (specification.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Remplir les champs", "Remplir les champs", 2);
        } else {

            Reclamation r = new Reclamation(specification.getText());
            CrudReclamation c = new CrudReclamation();
            c.UpdateReclamation(r, id);
            JOptionPane.showMessageDialog(null, "Modification avec succes", "Modification avec succes", 1);
           /* Parent tableViewParent = FXMLLoader.load(getClass().getResource("ListeReclamation.fxml"));
            Scene tableViewScene = new Scene(tableViewParent);

            //This line gets the Stage information
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

            window.setScene(tableViewScene);
            window.show();*/
        }
    }

}
