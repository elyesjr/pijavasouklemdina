/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class BackController implements Initializable {

    @FXML
    private Label eventback;
    @FXML
    private AnchorPane pane;
    @FXML
    private ImageView event;
    @FXML
    private ImageView reclam;
    @FXML
    private ImageView chart;
    @FXML
    private ImageView livraison;
    @FXML
    private ImageView demande;
    @FXML
    private ImageView logoutb;
    @FXML
    private AnchorPane panne11;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void event(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("Login.fxml"));
        pane.getChildren().setAll(Anchpane);
    }

    @FXML
    private void reclam(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("ListeReclamation.fxml"));
        pane.getChildren().setAll(Anchpane);
    }

    @FXML
    private void chart(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("ListeReclamation.fxml"));
        pane.getChildren().setAll(Anchpane);
    }

    @FXML
    private void livraison(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("ListeReclamation.fxml"));
        pane.getChildren().setAll(Anchpane);
    }

    @FXML
    private void demande(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("ListeReclamation.fxml"));
        pane.getChildren().setAll(Anchpane);
    }

    @FXML
    private void logoutb(MouseEvent event) throws IOException {
         AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("accueil.fxml"));
       panne11.getChildren().setAll(Anchpane);
        
    }
    
}
