/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

import static pidev.marwa.Presentation.ListeproduitpController.idpro;
import static pidev.GUI.LoginController.userc;
import pidev.marwa.entities.Avis;
import pidev.marwa.entities.ProduitPerso;
import static pidev.marwa.entities.user.id;
import pidev.marwa.services.AvisServices;
import pidev.marwa.util.DataSource;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class AfficherdetailController implements Initializable {

    @FXML
    private Label nompp;
    @FXML
    private Label prix;
    @FXML
    private Label description;
    @FXML
    private Label quantite;
    @FXML
    public ImageView imagedd;
    public File fileSelected = null;
    public String path;
    @FXML
    private Button retour;
    @FXML
    private AnchorPane rootpanne;
    @FXML
    private TextField message;
    @FXML
    private TableView<Avis> tableview1;
    @FXML
    private TableColumn<Avis, Integer> rat;
    @FXML
    private TableColumn<Avis, String> commentaire;
   
    public static double ratingvalue ;
    @FXML
    private Label leplusconsulter;
    @FXML
    private ImageView rating4;
   
    Connection cn = DataSource.getInstance().getConnection();
    @FXML
    private ImageView r3;
    @FXML
    private ImageView r2;
    @FXML
    private ImageView r1;
    @FXML
    private Button b1;
    @FXML
    private Button b2;
    @FXML
    private Button b3;
    @FXML
    private Button b4;
   

    
    public void setNompp(String nompp) {
        this.nompp.setText(nompp);
    }

    public void setPrix(double prix) {
        this.prix.setText(String.valueOf(prix));
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    /**
     * Initializes the controller class.
     */
    public void setQuantite(int quantite) {    
        this.quantite.setText(String.valueOf(quantite));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
           initcola();
        listavis();
//                type.getItems().addAll("1", "2", "3","4");

      
    }

//    

    @FXML
    private void retour(ActionEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
        
    }
 private void initcola(){
   rat.setCellValueFactory(new PropertyValueFactory<>("rating"));
   commentaire.setCellValueFactory(new PropertyValueFactory<>("commentaire"));
    
     }

      private void listavis()  {
    
          AvisServices av= new AvisServices();
                     List<Avis> list= av.ListerAvis();
                     tableview1.getItems().setAll(list);
      }
      @FXML
    private void ajouterrating1(ActionEvent event) {
         Avis a = new Avis(1,message.getText(),idpro,userc);
         
            
              AvisServices as= new AvisServices();
              as.ajouterAvis(a);
              int input = JOptionPane.showConfirmDialog(null, " merci pour votre avis");
     
              listavis();
    }
    @FXML
    private void ajouterrating2(ActionEvent event) {
         Avis a = new Avis(2,message.getText(),idpro,userc);
            
            
            
              AvisServices as= new AvisServices();
              as.ajouterAvis(a);
              int input = JOptionPane.showConfirmDialog(null, " merci pour votre avis");
     
              listavis();
    }
    @FXML
    private void ajouterrating3(ActionEvent event) {
         Avis a = new Avis(3,message.getText(),idpro,userc);
           
            
            
              AvisServices as= new AvisServices();
              as.ajouterAvis(a);
              int input = JOptionPane.showConfirmDialog(null, " merci pour votre avis");
     
              listavis();
    }
    @FXML
    private void ajouterrating4(ActionEvent event) {
         Avis a = new Avis(4,message.getText(),idpro,userc);
            
            
            
              AvisServices as= new AvisServices();
              as.ajouterAvis(a);
              int input = JOptionPane.showConfirmDialog(null, " merci pour votre avis");
     
              listavis();
    }

}


