/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import pidev.marwa.entities.ProduitPerso;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.services.CrudReclamation;
import pidev.marwa.util.DataSource;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class ListeReclamationController implements Initializable {

    @FXML
    private TableView<Reclamation> table;
    @FXML
    private TableColumn<Reclamation, String> idrec;
    @FXML
    private TableColumn<Reclamation, String> iduserrec;
     @FXML
    private TableColumn<Reclamation, String> idproduit;
    @FXML
    private TableColumn<Reclamation, String> specirec;
    @FXML
    private TableColumn<Reclamation, String> date;
    private ObservableList<Reclamation> data;
    @FXML
    private TextField rechercher;
    private DataSource con;
    @FXML
    private JFXButton delete;
    @FXML
    private JFXButton modiferREC;
    public static int idrecl;
    @FXML
    private Button trailter;
   
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        recherche();
        LoadData();
        
        /*  table.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetails(newValue));*///ligne selectionné
               
    }

    public void LoadData() {
        CrudReclamation CBS = new CrudReclamation();
        data = FXCollections.observableArrayList();
        List<Reclamation> list = new ArrayList<>();

        try {
            list = CBS.DisplayAllReclamation();
        } catch (SQLException ex) {
            Logger.getLogger(ListeReclamationController.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Reclamation b : list) {
            data.add(b);
        }

        idrec.setCellValueFactory(new PropertyValueFactory<>("idrec"));
        idproduit.setCellValueFactory(new PropertyValueFactory<>("idproduit"));
       iduserrec.setCellValueFactory(new PropertyValueFactory<>("idUser"));

        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        specirec.setCellValueFactory(new PropertyValueFactory<>("specification"));

        table.setItems(null);
        table.setItems(data);
    }

    @FXML
    public void recherche() {
        rechercher.setOnKeyReleased(e -> {
            if (rechercher.getText().equals("")) {
                LoadData();
            } else {
                data.clear();
                String sql = "SELECT * FROM `reclamation` WHERE `problem` LIKE  '%" + rechercher.getText() + "%' ";

                try {

                    PreparedStatement statement = con.getConnection().prepareStatement(sql);

                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {

                        data.add(new Reclamation(0, 8,8, rs.getString(4), rs.getString(5)));

                    }
                    table.setItems(data);
                } catch (SQLException ex) {
                    Logger.getLogger(ListeReclamationController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    @FXML
    void deleteact(ActionEvent event) throws SQLException {

        Reclamation reclamation= table.getSelectionModel().getSelectedItem();
        int input = JOptionPane.showConfirmDialog(null, "voulez vous confirmer suppression ?");
        if (input == 0) {
            CrudReclamation.getInstance().DeleteReclamation(reclamation.getId());
            LoadData();
        }
    }

    @FXML
    private void modifierrec(ActionEvent event)  throws IOException{
        ObservableList<Reclamation> selectedRows, all;
        all = table.getItems();

        selectedRows = table.getSelectionModel().getSelectedItems();

        for (Reclamation b : selectedRows) {
            try {

                idrecl = b.getId();
                System.out.println("" + idrecl);
                System.out.println("desole");
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("modrecla.fxml"));
                /* 
         * if "fx:controller" is not set in fxml
         * fxmlLoader.setController(NewWindowController);
                 */
                Scene scene = new Scene(fxmlLoader.load());
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();
                LoadData();
                // Hide this current window (if this is what you want)
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Id not figured");

            }

        }
    }

    @FXML
    private void traiterrec(ActionEvent event) throws SQLException {
       
         Reclamation reclamation= table.getSelectionModel().getSelectedItem();
        int input = JOptionPane.showConfirmDialog(null, "voulez vous confirmer suppression ?");
                       

        if (input == 0) {
             Statement ste;
            Connection cn = DataSource.getInstance().getConnection();
            ResultSet res;
            ProduitPerso p = new ProduitPerso();
            CrudReclamation cr = new CrudReclamation();
             String rech = "select * from produitp";
            ste = cn.createStatement();
            res = ste.executeQuery(rech);
            List<ProduitPerso> list = new ArrayList<>();
      
        while (res.next()) {
            ProduitPerso prod = new ProduitPerso(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getDouble(5), res.getString(6), res.getInt(7));
            list.add(prod);
        }
            System.out.println("zzzzz"+list);
for (ProduitPerso b : list) {
            cr.DeleteReclamation2(b.getId());
        }
       
       
            
            
            System.out.println("tssssst"+idrec);
            LoadData();
        }
            }

    }

