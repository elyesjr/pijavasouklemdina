/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import static pidev.marwa.Presentation.ListeReclamationController.idrecl;
import pidev.marwa.entities.ProduitPerso;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.services.CrudProduitPerso;
import pidev.marwa.services.CrudReclamation;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class ListeproduitpController implements Initializable {

    @FXML
    private TableView<ProduitPerso> table;
    @FXML
    private TableColumn<ProduitPerso, String> nom;
    @FXML
    private TableColumn<ProduitPerso, String> image;
    @FXML
    private TableColumn<ProduitPerso, String> prix;
    @FXML
    private TableColumn<ProduitPerso, String> description;
    private ObservableList<ProduitPerso> data;
    private Connection mycon;
    public static int  id;
    @FXML
    private TableColumn<ProduitPerso, String> idl;
    @FXML
    private TableColumn<ProduitPerso, String> updated_at;
    @FXML
    private TableColumn<ProduitPerso, String> quantite;
    @FXML
    private Button details;
    @FXML
    private Label nomd;
    @FXML
    private Label descriptiond;
    @FXML
    private Label imaged;
    @FXML
    private ImageView imagedd;
    @FXML
    private JFXButton btnretour;
    @FXML
    private AnchorPane rootpanne;
    @FXML
    private JFXButton btnSupprimer;
    @FXML
    private Button modifier;
    
        public static int idpro;
      

   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LoadData();
         
table.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
            try {
                showDetails(newValue);
            } catch (SQLException ex) {
                Logger.getLogger(ListeproduitpController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        // TODO
    }

    private void LoadData() {
        CrudProduitPerso CBS = new CrudProduitPerso();
        data = FXCollections.observableArrayList();
        List<ProduitPerso> list = new ArrayList<>();

        try {
            list = CBS.DisplayAllProduitPerso();
        } catch (SQLException ex) {
            Logger.getLogger(ListeproduitpController.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (ProduitPerso b : list) {
            data.add(b);
        }
        idl.setCellValueFactory(new PropertyValueFactory<>("id"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        image.setCellValueFactory(new PropertyValueFactory<>("image_name"));
        prix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        updated_at.setCellValueFactory(new PropertyValueFactory<>("updated_at"));
        quantite.setCellValueFactory(new PropertyValueFactory<>("quantite"
                + ".0..t.3e"));

        table.setItems(null);
        table.setItems(data);
    }
    private void showDetails(ProduitPerso ProduitPerso) throws SQLException {
        if (ProduitPerso == null) {
            
            
//            System.out.println("" + ProduitPerso.getNom());

            nomd.setText("");
            descriptiond.setText("");
        } else {
            
         //   System.out.println("" + ProduitPerso.getNom());
            nomd.setText(ProduitPerso.getNom());
            descriptiond.setText(ProduitPerso.getDescription());
            File file = new File(ProduitPerso.getImage_name());

            Image image = new Image(file.toURI().toString());

            imagedd.setImage(image);

        }
    }

 @FXML
    public void afficherDetails(ActionEvent event) throws IOException {
           
        ObservableList<ProduitPerso> selectedRows, all;
        all = table.getItems();

        
        selectedRows = table.getSelectionModel().getSelectedItems();

        
        for (ProduitPerso b : selectedRows) {
         
                idpro = b.getId();
                nomd.setText(b.getNom());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("afficherdetail.fxml"));
                Parent root=fxmlLoader.load();
                AfficherdetailController aff=fxmlLoader.getController();
                aff.setDescription(b.getDescription());
                aff.setNompp(b.getNom());
                aff.setPrix(b.getPrix());
                aff.setQuantite(b.getQuantité());
                  File file = new File(b.getImage_name());

            Image image = new Image(file.toURI().toString());

                aff.imagedd.setImage(imagedd.getImage());
//                Scene scene = new Scene(fxmlLoader.load());
//                Stage stage = new Stage();
//
//                stage.setScene(scene);
//                stage.show();
table.getScene().setRoot(root);
               
          

        }
    }

    
    @FXML
    private void premierPage (ActionEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

    @FXML
  private void deleteact(ActionEvent event) throws SQLException {
       
            System.out.println("marwa");
            
            ProduitPerso produiterso = table.getSelectionModel().getSelectedItem();
            
            
            
             int input = JOptionPane.showConfirmDialog(null, "voulez vous confirmer suppression ?");
        if (input == 0) {

           CrudProduitPerso.getInstance().DeleteProduitPerso(produiterso.getId());


        LoadData();
           
        }else{
        LoadData();}
            
        
    }

    @FXML
    private void modifier(ActionEvent event) throws IOException {
        
        ObservableList<ProduitPerso> selectedRows, all;
        all = table.getItems();

        selectedRows = table.getSelectionModel().getSelectedItems();

        for (ProduitPerso b : selectedRows) {
            try {

                idpro = b.getId();
                System.out.println("" + idpro);
                System.out.println("desole");
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("Modifierproduitp.fxml"));
                /* 
         * if "fx:controller" is not set in fxml
         * fxmlLoader.setController(NewWindowController);
                 */
                Scene scene;
                scene = new Scene(fxmlLoader.load());
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();
                // Hide this current window (if this is what you want)
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Id not figured");

            }
    

}
    }
}
