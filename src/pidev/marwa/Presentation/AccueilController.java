/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class AccueilController implements Initializable {

    @FXML
    private ImageView login;
    @FXML
    private ImageView produitp;
    @FXML
    private ImageView boutique;
    @FXML
    private ImageView logout;
    @FXML
    private ImageView evenement;
    @FXML
    private AnchorPane rootpanne;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void login(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("Login.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
        
    }

    @FXML
    private void produitp(MouseEvent event) throws IOException {
         AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

    @FXML
    private void boutique(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

    @FXML
    private void logout(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }

    @FXML
    private void evenement(MouseEvent event) throws IOException {
        AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }
    
}
