/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.Presentation;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;
import static pidev.marwa.Presentation.ListeReclamationController.idrecl;
import static pidev.marwa.Presentation.ListeproduitpController.id;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.services.CrudReclamation;
import pidev.marwa.util.DataSource;

/**
 * FXML Controller class
 *
 * @author gaalo
 */
public class ModreclaController implements Initializable {

    @FXML
    private TextArea specification;
    @FXML
    private Button modifier;
    @FXML
    private DatePicker date;
 private DataSource mycon;
    @FXML
    private Button btnretour;
    @FXML
    private AnchorPane rootpanne;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
     mycon = new DataSource();
     LoadData();
    }   
    
     private void LoadData() {
         Connection con = mycon.getConnection();
        Reclamation e = new Reclamation();
        try {
           
            ResultSet rs = con.createStatement().executeQuery("SELECT * FROM `reclamation` WHERE `id_reclamation`= " + idrecl + " ");
            while (rs.next()) {
                e = new Reclamation(rs.getString(5));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeReclamationController.class.getName()).log(Level.SEVERE, null, ex);

        }
        specification.setText(e.getSpecification());
    }

    
    
    
    
    @FXML

    private void modifier(ActionEvent event) throws SQLException, IOException {

        if (specification.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Remplir les champs", "Remplir les champs", 2);
        } else {
CrudReclamation c = new CrudReclamation();
            Reclamation r = new Reclamation(specification.getText());
            System.out.println(idrecl);
            
            c.UpdateReclamation(r, idrecl);
            JOptionPane.showMessageDialog(null, "Modification avec succes", "Modification avec succes", 1);
           /* Parent tableViewParent = FXMLLoader.load(getClass().getResource("ListeReclamation.fxml"));
            Scene tableViewScene = new Scene(tableViewParent);

            //This line gets the Stage information
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

            window.setScene(tableViewScene);
            window.show();*/
        }
    }

    @FXML
    private void premierPage(ActionEvent event) throws IOException {
         AnchorPane Anchpane = FXMLLoader.load(getClass().getResource("PremierPage.fxml"));
        rootpanne.getChildren().setAll(Anchpane);
    }
}
