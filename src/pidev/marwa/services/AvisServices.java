/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static pidev.marwa.Presentation.ListeproduitpController.idpro;
import pidev.marwa.entities.Avis;
import pidev.marwa.util.DataSource;

/**
 *
 * @author Touha
 */
public class AvisServices {

    Connection cn = DataSource.getInstance().getConnection();

    public void ajouterAvis(Avis a) {
        try {
            String requete = "insert into rating2 (user,rating,avis,idproduit) values(?,?,?,?)";
            PreparedStatement st = cn.prepareStatement(requete);
            st.setInt(1, a.getUser());
            st.setInt(2, a.getRating());
            st.setString(3, a.getCommentaire());
            st.setInt(4, a.getIdproduit());

            st.executeUpdate();
            System.out.println("Avis ajouter");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public List<Avis> ListerAvis() {
        List<Avis> mylist = new ArrayList<Avis>();

        try {
            String requete2 = "SELECT *FROM rating2 where idproduit =" + idpro;
            Statement st2 = cn.createStatement();
            ResultSet rs = st2.executeQuery(requete2);
            while (rs.next()) {
                Avis a = new Avis();
                a.setId(rs.getInt(1));
                a.setRating(rs.getInt(4));
                a.setCommentaire(rs.getString(5));
                mylist.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AvisServices.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mylist;

    }

    public float CalculateProductRate(int idproduitp) throws SQLException {

        int numberOfRates = 0;
        float sumOfRates = 0;

        String req = "SELECT * FROM rating2 WHERE idproduitp=?";
        PreparedStatement st = cn.prepareStatement(req);
        ResultSet rs = st.executeQuery(req);

        st.setInt(1, idproduitp);

        rs = st.executeQuery();

        while (rs.next()) {
            numberOfRates++;
            sumOfRates += rs.getFloat("value");
        }

        return (sumOfRates / numberOfRates);
    }
}
