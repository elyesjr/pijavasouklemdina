/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.util.DataSource;

/**
 *
 * @author Nihel
 */
public class CrudReclamation implements ICrudReclamation {

    Statement ste;
    Connection cn = DataSource.getInstance().getConnection();
    PreparedStatement pst;
    ResultSet res;
    private static CrudReclamation instance;

    public CrudReclamation() {
    }

    public static CrudReclamation getInstance() {
        if (instance == null) {
            instance = new CrudReclamation();
        }
        return instance;
    }

    public void ajouterReclamation(Reclamation object) {

        DataSource cn = DataSource.getInstance();

        Reclamation obj = (Reclamation) object;

        String req = "INSERT INTO `reclamation`(`id_reclamation`, `produitp`, `id_user`, `date`, `description`) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement statement = cn.getConnection().prepareStatement(req);

            statement.setInt(1, object.getId());
            statement.setInt(2, 1);
            statement.setInt(3, 8);
            statement.setString(4, object.getDate());
            statement.setString(5, object.getSpecification());

            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void DeleteReclamation(int id) throws SQLException {
        String requete = "Delete from reclamation where id_reclamation =" + id;
        ste = cn.createStatement();
        ste.executeUpdate(requete);
    }

    public void UpdateReclamation(Reclamation r, int id) throws SQLException {

        String requete = "UPDATE `reclamation` SET `description`=? WHERE id_reclamation=" + id;

        pst = cn.prepareStatement(requete);

        pst.setString(1, r.getSpecification());

        pst.executeUpdate();

    }

    public List<Reclamation> DisplayAllReclamation() throws SQLException {

        String requete = "Select * from reclamation  order by id_reclamation DESC";
        List<Reclamation> list = new ArrayList<>();
        ste = cn.createStatement();
        res = ste.executeQuery(requete);
        while (res.next()) {
            Reclamation p = new Reclamation(res.getInt(1), res.getInt(2), res.getInt(3), res.getString(4), res.getString(5));
            list.add(p);
        }
        System.out.println("" + list.toString());
        return list;

    }

    public List<Reclamation> DisplayAllReclamation2(int id) throws SQLException {

        String requete = "Select * from reclamation  where id_reclamation =" + id;
        List<Reclamation> list = new ArrayList<>();
        ste = cn.createStatement();
        res = ste.executeQuery(requete);
        while (res.next()) {
            Reclamation p = new Reclamation(res.getInt(1), res.getInt(2), res.getInt(3), res.getString(4), res.getString(5));
            list.add(p);
        }
        System.out.println("" + list.toString());
        return list;

    }

    public void DeleteReclamation2(int id) throws SQLException {
        System.out.println("iddddbouk"+id);
        String rech = "select * from reclamation where `produitp`=" + id;
        ste = cn.createStatement();
        res = ste.executeQuery(rech);

        int nbr = 0;
        while (res.next()) {
            nbr = nbr + 1;
    }
        System.out.println("nnnn"+nbr);
         if (nbr >= 5) {
                String requete = "Delete from produitp where id =" +id ;
                String requete2 = "Delete from reclamation where `produitp` =" +id ;
                
                ste = cn.createStatement();
                ste.executeUpdate(requete);
                ste.executeUpdate(requete2);

        }

    }
}
