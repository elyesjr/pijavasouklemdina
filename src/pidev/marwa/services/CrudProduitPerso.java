/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.marwa.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pidev.marwa.entities.ProduitPerso;
import pidev.marwa.entities.Reclamation;
import pidev.marwa.util.DataSource;

/**
 *
 * @author gaalo
 */
public class CrudProduitPerso implements ICrudProduitPerso {
public static int id_prod;
    Statement ste;
    Connection cn = DataSource.getInstance().getConnection();
    PreparedStatement pst;
    ResultSet res;
    private static CrudProduitPerso instance;

    public CrudProduitPerso() {
    }

    public static CrudProduitPerso getInstance() {
        if (instance == null) {
            instance = new CrudProduitPerso();
        }
        return instance;
    }

    public void ajouterProduitPerso(ProduitPerso rec) {

        String req = "INSERT INTO `produitp` "
                + "(`nom`,`image_name`,`prix`,`description`)"
                + " values (?,?,?,?)";

        try {
            PreparedStatement statement = cn.prepareStatement(req);
         //   statement.setInt(1, 0);
            statement.setString(1, rec.getNom());
            statement.setString(2, rec.getImage_name());
            statement.setDouble(3, rec.getPrix());
            statement.setString(4, rec.getDescription());
         //   statement.setInt(5, rec.getQuantité());

            statement.executeUpdate();
//            ste.executeUpdate(req,Statement.RETURN_GENERATED_KEYS);
//        ResultSet rs = ste.getGeneratedKeys();
//            
//if(rs.next()) {
//      id_prod=rs.getInt(1);
//        System.out.println("id_user = "+id_prod);
//      //what you get is only a RowId ref, try make use of it anyway U could think of
//    }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
 public List<ProduitPerso> DisplayAllProduitPerso() throws SQLException{
    
        String requete = "Select * from produitp ";
        List<ProduitPerso> list = new ArrayList<>();
        ste=cn.createStatement();
        res=ste.executeQuery(requete);
        while(res.next())
        {
            ProduitPerso p = new ProduitPerso(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getDouble(5), res.getString(6), res.getInt(7));
            list.add(p);
        }
        System.out.println(""+list.toString());
        return list;
        
    }
 
 public void  deleteRaiting(int id)
 {
     try{
         String req="DELETE  FROM `rating2` WHERE idproduit="+id ;
          ste=cn.createStatement();
        ste.executeUpdate(req);
     }catch(SQLException ex){
    ex.printStackTrace();
     }
    
 }
  public void  deleteReclamation(int id)
 {
     try{
         String req="DELETE FROM `reclamation` WHERE produitp="+id ;
          ste=cn.createStatement();
        ste.executeUpdate(req);
     }catch(SQLException ex){
    ex.printStackTrace();
     }
    
 }
    public void DeleteProduitPerso(int id) throws SQLException{
        try{
            String requete = "Delete from produitp where id =" +id ;
        
    
        ste=cn.createStatement();
        ste.executeUpdate(requete);
    }catch(SQLException ex){
    ex.printStackTrace();}
    }
   public void UpdateProduitPerso(ProduitPerso r,int id) throws SQLException{
        String requete =  "UPDATE `produitp` SET `nom`=?,`image_name`=?, `prix`=?, `description`=? WHERE id=" +id;
       
        pst=cn.prepareStatement(requete);
        pst.setString(1, r.getNom());
        pst.setString(2, r.getImage_name());
        pst.setDouble(3, r.getPrix());
        pst.setString(4, r.getDescription());


        pst.executeUpdate();
        
    }
}
