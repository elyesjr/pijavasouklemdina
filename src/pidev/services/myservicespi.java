/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pidev.GUI.LoginController;

import pidev.entities.Abonnement;
import pidev.entities.artisan;
import pidev.entities.boutique;
import pidev.entities.notification;
import pidev.utils.myconnexionpi;

/**
 *
 * @author MBM info
 */
public class myservicespi {
    Connection cn= myconnexionpi.getInstance().getConnection();
    
    
    public void ajouterBoutique(boutique b) {

        try {
            String insert = "INSERT INTO boutique (user_id,nom_bout,adresse,image,numtel)VALUES (?,?,?,?,?);";
            PreparedStatement st1 = cn.prepareStatement(insert);
            st1.setInt(1, b.getUserid());
             st1.setString(3,b.getAdresse());
             st1.setString(5,b.getNumtel());
             st1.setString(2,b.getNombout());
             st1.setString(4,b.getImage());

             
            st1.executeUpdate();
            System.out.println("boutique " + b.getNombout() + " ajoutée !!!");

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

    }
    public void ajouterNote(boutique b) {

        try { // LES var declaré dans le try ne sont vue que dans le try, et inversement pour en dhors du try
            String requete = "INSERT INTO boutique(adresse,nom_bout,numtel,image) VALUES('" + b.getAdresse() + "','" + b.getNombout()+ "','" + b.getNumtel() + "','" + b.getImage() + "')"; //MAJUSCULE NON OBLIGATOIRE 
            Statement st = cn.createStatement(); // import java.sql.Statement
            st.executeUpdate(requete);
            System.out.println("note ajoutée");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

    }
    
    
     
/*     public void ajouterboutique2 (boutique b){
         
         try {
             String requete="insert into Personne (adresse,numtel,nombout,image,user_id) values (?,?,?,?,?)";
             PreparedStatement st =cn.prepareStatement(requete);
         
             st.setString(1,b.getAdresse());
             st.setString(2,b.getNumtel());
             st.setString(3,b.getNombout());
             st.setString(4,b.getImage());
             
            
             st.executeUpdate(requete);
             System.out.println("boutique ajoutée");
         } catch (SQLException ex) {
System.err.print(ex.getMessage());         }
    }*/
     public List<boutique>listerboutiques(){
       List<boutique>mylist=new ArrayList<boutique>();
         try {
         
           String requete2="select * from boutique";
           Statement st2=cn.createStatement();
           
           ResultSet rs =st2.executeQuery(requete2);
           while(rs.next()){
               boutique b=new boutique();
               b.setId(rs.getInt(1));
                b.setUserid(rs.getInt(2));
               b.setNombout(rs.getString(3));
               b.setAdresse(rs.getString(4));
               b.setNumtel (rs.getString(6));
            
             b.setImage(rs.getString(5));
               mylist.add(b);
           }}
       
        catch (SQLException ex) {
           Logger.getLogger(myservicespi.class.getName()).log(Level.SEVERE, null, ex);
                  }
         return mylist;}
      
    public void supprimerBoutique(int id) {
            String deleteBoutique = "DELETE FROM `boutique`  WHERE id= ? " ;
            String deleteProduit = "DELETE FROM produit WHERE boutique_id= ?";
            String deleteAbonnement = "DELETE FROM abonnement WHERE id_boutique= ?";
            
            class deleteB{
                public void delete(String request)throws SQLException{
                    
                    PreparedStatement st2 = cn.prepareStatement(request);
                    st2.setInt(1, id);
                    st2.executeUpdate();
                }
            }
            
        try {
            deleteB dlb = new deleteB();
            dlb.delete(deleteAbonnement);
            dlb.delete(deleteProduit);
            dlb.delete(deleteBoutique);
            System.out.print("boutique supprimé");

        } catch (SQLException e) {

            System.err.println("SQLException: " + e.getMessage());
        }

    }
    
  public void ModifierBoutique(boutique b ,int id) {

        try {
            String update = "UPDATE boutique  SET  nom_bout = ?, numtel = ? , adresse = ? , image=? WHERE id = ? ;";

            PreparedStatement statement2 = cn.prepareStatement(update);
           
            statement2.setString(1, b.getNombout());
            statement2.setString(2, b.getNumtel());
            statement2.setString(3, b.getAdresse());
          statement2.setString(4, b.getImage());
           
           statement2.setInt(5, id);
           

            statement2.executeUpdate();
            System.err.println("boutique modifié");
        } catch (SQLException e) {
            System.err.println("boutique " + b.getId() + " non modifiee");
            System.out.println(e.getMessage());
        }

    }  
  
  
  
//  
//  public List<boutique> rechercheboutique(String nombout)
//    {
//        List<boutique> list =new ArrayList<>();
//        String requete="SELECT * FROM boutique WHERE nom_bout= '"+nombout+"'";
//         try{
//            Statement st2=cn.createStatement();
//         ResultSet rs =st2.executeQuery(requete);
//         while(rs.next()){
//                 boutique b = new boutique();
//                     e.setId(rs.getInt(1));
//             e.setEvenement_user(rs.getInt(2));
//             e.setNom(rs.getString(3));
//             e.setDate_debut(rs.getDate(4));
//             e.setDate_fin(rs.getDate(5));
//             e.setImage(rs.getString(6));
//             e.setLieu(rs.getString(7));
//              e.setDescription(rs.getString(8));
//               e.setEtat(rs.getInt(9));
//             list.add(e);
//             }
//         }catch(SQLException ex){
//      ex.printStackTrace();
//  }
//                 
//        return list;
//    }
    public ObservableList<boutique> rechercherBoutique(String nombout) {

        ObservableList<boutique> ls = FXCollections.observableArrayList();
        boutique b = new boutique();
        try {
            String select = "SELECT * FROM boutique WHERE nom_bout = '" + nombout + "' ";
            Statement statement1 = cn.createStatement();
            ResultSet result = statement1.executeQuery(select);

            while (result.next()) {
                b.setId(result.getInt(1));
                 b.setUserid(result.getInt(2));
                b.setNombout(result.getString(3));
                b.setAdresse(result.getString(4));
                b.setNumtel(result.getString(6));
                b.setImage(result.getString(5));
               
                ls.add(b);

            }
        } catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage());
            System.err.println("SQLSTATE: " + e.getSQLState());
            System.err.println("VnedorError: " + e.getErrorCode());
        }
        return ls;

    }
    
    
    
    
    
    
    public void ajouterArtisan(artisan a) {

        try {
            String insert = "INSERT INTO artisan (user_id,adresse,numtel,nomart,image)VALUES (?,?,?,?,?);";
            PreparedStatement st1 = cn.prepareStatement(insert);
             st1.setInt(1,a.getUser_id());
            st1.setString(2,a.getAdresse());
             st1.setString(3,a.getNumtel());
             st1.setString(4,a.getNomart());
             st1.setString(5,a.getImage());

            st1.executeUpdate();
            System.out.println("artisan " + a.getNomart() + " ajoutée !!!");

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

    }
    
    
    
     
     
     public List<artisan>listerartisans(){
       List<artisan>list=new ArrayList<artisan>();
         try {
         
           String requete2="select * from artisan";
           Statement st2=cn.createStatement();
           
           ResultSet rs =st2.executeQuery(requete2);
           while(rs.next()){
               artisan a=new artisan();
               a.setId(rs.getInt(1));
               
               a.setUser_id(rs.getInt(2));
               a.setNomart(rs.getString(5));
               a.setAdresse(rs.getString(4));
               a.setNumtel (rs.getString(3));
            
             a.setImage(rs.getString(6));
               list.add(a);
           }}
       
        catch (SQLException ex) {
           Logger.getLogger(myservicespi.class.getName()).log(Level.SEVERE, null, ex);
                  }
         return list;}
      
    public void supprimerartisan(int id) {
 // String delete = "DELETE FROM `artisan` WHERE id= ?" ;
   String deleteArtisan = "DELETE FROM `artisan`  WHERE id= ? " ;
            String deleteProduit = "DELETE FROM produit WHERE boutique_id= ?";
           
            
            class deleteB{
                public void delete(String request)throws SQLException{
                    
                    PreparedStatement st2 = cn.prepareStatement(request);
                    st2.setInt(1, id);
                    st2.executeUpdate();
                }
            }
            
        try {
            deleteB dlb = new deleteB();
            
            dlb.delete(deleteProduit);
            dlb.delete(deleteArtisan);
            System.out.print("boutique supprimé");

        }

        catch (SQLException e) {

            System.err.println("SQLException: " + e.getMessage());
        }

    }
    
    
    
    
    
    
  public void Modifierartisan(artisan a,int id) {

        try {
            String update = "UPDATE artisan  SET nomart = ?, numtel = ? , adresse = ?, image = ? WHERE id = ? ;";

            PreparedStatement statement2 = cn.prepareStatement(update);
            statement2.setString(1, a.getNomart());
            statement2.setString(2, a.getNumtel());
            statement2.setString(3, a.getAdresse());
            statement2.setString(4, a.getImage());
             statement2.setInt(5, id);
           

            statement2.executeUpdate();
             System.err.println("artisan modifier");

        } catch (SQLException e) {
            System.err.println("artisan" + a.getNomart()+ " non modifiee");
            System.out.println(e.getMessage());
        }
        





    }  
  
    public List<artisan> rechercherartisan(String nomart) {
ObservableList<artisan> ls = FXCollections.observableArrayList();
         artisan a = new artisan();
        try {
            String select = "SELECT * FROM artisan WHERE nomart = '" + nomart + "' ";
            Statement statement1 = cn.createStatement();
            ResultSet result = statement1.executeQuery(select);

            while (result.next()) {
                 a.setId(result.getInt(1));
                 a.setUser_id(result.getInt(2));
                a.setAdresse(result.getString(3));
                a.setNumtel(result.getString(4));
                a.setNomart(result.getString(5));
                a.setImage(result.getString(6));
                 
               
                ls.add(a);

            }
        } catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage());
            System.err.println("SQLSTATE: " + e.getSQLState());
            System.err.println("VnedorError: " + e.getErrorCode());
        }
        return ls;

       
    }
    
    public void ajouterAbonnement(Abonnement ab)
    {
        String req="INSERT INTO `Abonnement`(`id`,`id_user`, `id_boutique`) VALUES (?,?,?)";
        
        try{
          
        
        
        PreparedStatement statemnt =cn.prepareStatement(req);
       statemnt.setInt(1,ab.getId());
        statemnt.setInt(2,ab.getId_user());
        statemnt.setInt(3,ab.getId_boutique() );
        
      
        statemnt.executeUpdate();
            System.out.println("abonnement ajouté");
        }catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage());
            System.err.println("SQLSTATE: " + e.getSQLState());
            System.err.println("VnedorError: " + e.getErrorCode());
        }
    }
    public void supprimerAbonnement(int id_user , int id_boutique)
    {
        String req="DELETE FROM `Abonnement` WHERE id_user=? and id_boutique=?";
        try{
        PreparedStatement statement=cn.prepareStatement(req);

        statement.setInt(1, id_user);
          statement.setInt(2, id_boutique);
          
        statement.executeUpdate();
         System.out.println("abonnement supprimé");
        }catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage());
            System.err.println("SQLSTATE: " + e.getSQLState());
            System.err.println("VnedorError: " + e.getErrorCode());
        }
}
    
    
     public List<Abonnement> GetAbonnementsByuserId(int id) throws SQLException {

        List<Abonnement> listAbonnement = new ArrayList<Abonnement>();
        Abonnement abonnement = new Abonnement();
         
   
       
      
            String req = "SELECT * FROM Abonnement WHERE id_user=?";
             PreparedStatement pst=cn.prepareStatement(req);
        
            pst.setInt(1, id);
        ResultSet rs = pst.executeQuery();
            while (rs.next()){
                Abonnement abonnement1 = new Abonnement();
            abonnement1.setId_user(rs.getInt("iduser"));
            abonnement1.setId_boutique(rs.getInt("idboutique"));
                   listAbonnement.add(abonnement1); }
        
        return listAbonnement;

    }
    
     public List<Abonnement> GetAbonnementsByMembreId(int id) throws SQLException {

        List<Abonnement> listAbonnement = new ArrayList<Abonnement>();
        Abonnement abonnement = new Abonnement(id, id);
        
            String req = "SELECT * FROM Abonnement WHERE id_user=?";
            PreparedStatement pst=cn.prepareStatement(req);
            pst.setInt(1, id);
        ResultSet rs = pst.executeQuery();
            while (rs.next()){
                Abonnement abonnement1 = new Abonnement();
            abonnement1.setId_user(rs.getInt("iduser"));
            abonnement1.setId_boutique(rs.getInt("idboutique"));
                   listAbonnement.add(abonnement1); }
        
        return listAbonnement;

    }
     
      public boolean isAbonne(int id_user,int id_boutique) throws SQLException {
            String req="SELECT * FROM Abonnement WHERE id_user=? and id_boutique=?";
   
        PreparedStatement pst=cn.prepareStatement(req);
      
          
            pst.setInt(1, id_user);
            pst.setInt(2, id_boutique);
        ResultSet rs = pst.executeQuery();
           
            
            
        while (rs.next()) {
            return true;
        }

        return false;

    }
    public boutique detailBoutique(int id)
    {
         boutique b=new boutique();
         try {
         
           String requete2="select * from boutique where id="+id;
           Statement st2=cn.createStatement();
           
           ResultSet rs =st2.executeQuery(requete2);
           while(rs.next()){
              
              // b.setId(rs.getInt(1));
               b.setNombout(rs.getString(3));
               b.setAdresse(rs.getString(4));
               b.setNumtel (rs.getString(6));
            
             b.setImage(rs.getString(5));
             
           }}
       
        catch (SQLException ex) {
           Logger.getLogger(myservicespi.class.getName()).log(Level.SEVERE, null, ex);
                  }
         return b ;
    }
    
    
    
     public artisan detailartisan(int id)
    {
         artisan a=new artisan();
         try {
         
           String requete2="select * from artisan where id="+id;
           Statement st2=cn.createStatement();
           
           ResultSet rs =st2.executeQuery(requete2);
           while(rs.next()){
              
              // b.setId(rs.getInt(1));
               a.setNomart(rs.getString(5));
               a.setAdresse(rs.getString(3));
               a.setNumtel (rs.getString(4));
            
             a.setImage(rs.getString(6));
             
           }}
       
        catch (SQLException ex) {
           Logger.getLogger(myservicespi.class.getName()).log(Level.SEVERE, null, ex);
                  }
         return a ;
    }
         public void ajouterNotification(notification notif)
    {
        String req="INSERT INTO `notification`(`user_id`,`message`,`link`,`isRead`,`addedAt`,`boutique_id`) VALUES (?,?,?,?,?,?)";
        
        try{
          
        
        
        PreparedStatement statemnt =cn.prepareStatement(req);
        
        statemnt.setInt(1,notif.getUser_id());
        statemnt.setString(2,notif.getMessage() );
        statemnt.setString(3,notif.getLink() );
        statemnt.setBoolean(4,notif.isRead );
        statemnt.setDate(5,notif.getAddedAt());
        statemnt.setInt(6, notif.getIdboutique());
        
      
        statemnt.executeUpdate();
            System.out.println("notification ajouté");
        }catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage());
            System.err.println("SQLSTATE: " + e.getSQLState());
            System.err.println("VnedorError: " + e.getErrorCode());
        }
    }
//         public boutique getBoutiqueByID(int id){
//             boutique btq = new boutique();
//        
//            String req = "SELECT * FROM boutique WHERE id=?";
//            PreparedStatement pst=cn.prepareStatement(req);
//            pst.setInt(1, id);
//            ResultSet rs = pst.executeQuery();
//            while (rs.next()){
//                btq.setAdresse(rs.getString("adresse"));
//                btq.setImage(rs.getString("image"));
//                btq.setNumtel(rs.getInt("numtel"));
//                btq.setNombout(rs.getString("nom_bout"));
//                btq.setUserid(rs.getInt("user_id"));
//            }
//        
//        return btq;
//         }
         //liste qui contient les id des boutiques
           public List<notification>listernotif(List<Integer> btqIDS){
               
       List<notification>list = new ArrayList<notification>();
         try {
         
           String requete2="select * from notification where isRead=0 " ;
           Statement st2=cn.createStatement();
           
           ResultSet rs =st2.executeQuery(requete2);
          
           while(rs.next()){
               //filtrage par boutique id
               if (btqIDS.contains(rs.getInt(7))){
               notification notif = new notification();
               notif.setId(rs.getInt(1));
               
               notif.setUser_id(rs.getInt(2));
               notif.setAddedAt(rs.getDate(6));
               notif.setId(rs.getInt(1));
               notif.setMessage(rs.getString(3));
               notif.setLink(rs.getString(4));
               notif.setIdboutique(rs.getInt(7));
            
               list.add(notif);
               }
                   
           }}
       
        catch (SQLException ex) {
           Logger.getLogger(myservicespi.class.getName()).log(Level.SEVERE, null, ex);
                  }
         return list;}
           
           public List<Integer> getUserBoutiquesIDs() throws SQLException {
               List<Integer> btqList = new ArrayList<Integer>();
                try{
                String requete2="select * from boutique where user_id=? " ;
                PreparedStatement st2=cn.prepareStatement(requete2);
                st2.setInt(1, LoginController.userc);
                
                ResultSet rs =st2.executeQuery();
                
                   while (rs.next()) {                       
                       btqList.add(rs.getInt(1));
                   }
                }catch(SQLException ex){};
                
               return btqList;
           }
         
    
  public void SetRead(int id) {

        try {
            String update = "UPDATE notification  SET isRead = 1 where id="+id;

            PreparedStatement statement2 = cn.prepareStatement(update);
        
            statement2.executeUpdate();
             System.err.println("artisan modifier");

        } catch (SQLException e) {
            System.err.println("notification vu");
            System.out.println(e.getMessage());
        }
  }
}