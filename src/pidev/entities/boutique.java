/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.entities;

import java.util.Objects;

/**
 *
 * @author MBM info
 */
public class boutique {
   public int id;
 public String adresse;
 public String numtel;
 public String nombout;
 public String image;
 public int userid;

    public boutique() {
    }
    

    public boutique(int id, String adresse, String numtel, String nombout, String image, int userid) {
        this.id = id;
        this.adresse = adresse;
        this.numtel = numtel;
        this.nombout = nombout;
        this.image = image;
        this.userid = userid;
    }

    public boutique(String adresse, String numtel, String nombout, String image, int userid) {
        this.adresse = adresse;
        this.numtel = numtel;
        this.nombout = nombout;
        this.image = image;
        this.userid = userid;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.adresse);
        hash = 29 * hash + Objects.hashCode(this.numtel);
        hash = 29 * hash + Objects.hashCode(this.nombout);
        hash = 29 * hash + Objects.hashCode(this.image);
        hash = 29 * hash + this.userid;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final boutique other = (boutique) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.userid != other.userid) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.numtel, other.numtel)) {
            return false;
        }
        if (!Objects.equals(this.nombout, other.nombout)) {
            return false;
        }
        if (!Objects.equals(this.image, other.image)) {
            return false;
        }
        return true;
    }
     public boutique(String adresse,String numtel, String nombout, String image) {
      
        this.adresse = adresse;
        this.numtel = numtel;
        this.nombout = nombout;
        this.image = image;
       
    }

    public boutique(int id) {
        this.id = id;
    }

    public boutique(String text, String text0, String text1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boutique(int aInt, String string, String string0, String string1, String string2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumtel() {
        return numtel;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public String getNombout() {
        return nombout;
    }

    public void setNombout(String nombout) {
        this.nombout = nombout;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

       public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        return "boutique{" + "id=" + id + ", adresse=" + adresse + ", numtel=" + numtel + ", nombout=" + nombout + ", image=" + image + ", user_id=" + userid + '}';
    }
  
}
