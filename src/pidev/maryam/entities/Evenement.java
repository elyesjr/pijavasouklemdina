/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.entities;

import java.sql.Date;



/**
 *
 * @author ASUS
 */
public class Evenement {
    public int nbrPlace;
    public int evenement_user ;
    public int id_event;
    public int etat;
    public String nom;
    public String lieu;
    public String description;
    
    public Date date_debut;
    public Date date_fin;
    public String image;

   

    public int getEvenement_user() {
        return evenement_user;
    }

    public void setEvenement_user(int evenement_user) {
        this.evenement_user = evenement_user;
    }

    public int getId_event() {
        return id_event;
    }

    public void setId_event(int id_event) {
        this.id_event = id_event;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Evenement() {
    }

    public int getNbrPlace() {
        return nbrPlace;
    }

    public void setNbrPlace(int nbrPlace) {
        this.nbrPlace = nbrPlace;
    }

    public Evenement(int evenement_user, int id_event, int etat, String nom, String lieu, String description, Date date_debut, Date date_fin, String image,int nbrPlace) {
        this.evenement_user = evenement_user;
        this.id_event = id_event;
        this.etat = etat;
        this.nom = nom;
        this.lieu = lieu;
        this.description = description;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.image = image;
        this.nbrPlace=nbrPlace;
    }
  public Evenement(int evenement_user, String nom, String description, Date date_debut, Date date_fin, String lieu, String image,int nbrPlace) {
        this.evenement_user = evenement_user;
       
      
        this.nom = nom;
        
        this.description = description;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.lieu = lieu;
        this.image = image;
        this.nbrPlace=nbrPlace;
    }
    @Override
    public String toString() {
        return "Evenement{" + "evenement_user=" + evenement_user + ", id_event=" + id_event + ", etat=" + etat + ", nom=" + nom + ", lieu=" + lieu + ", description=" + description + ", date_debut=" + date_debut + ", date_fin=" + date_fin + ", image=" + image + '}';
    }

  
  
    
}
