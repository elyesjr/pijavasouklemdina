/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class MyConnexion {
     public static MyConnexion instance;
    public Connection cnx;

    private MyConnexion() {
        try {
            String url = "jdbc:mysql://localhost:3306/souklemdinafinal";
            String login = "root";
            String pwd = "";

            //etablir la connextion avec la base
            cnx = DriverManager.getConnection(url, login, pwd);
            System.out.println("Connexion établie");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

    }

    public Connection getConnection() {
        return cnx;
    }

    public static MyConnexion getInstance() {
        if (instance == null) {
            instance = new MyConnexion();
        }
        return instance;
    }

  
}
