/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.interfaces;

import pidev.maryam.entities.Evenement;
import pidev.maryam.entities.Reservation;
import pidev.maryam.entities.user;
import static pidev.maryam.interfaces.AfficheDetailEvenementController.idEvent;
import pidev.maryam.services.CrudEvent;
import pidev.maryam.services.CrudReservation;
import pidev.maryam.services.userReservation;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import pidev.GUI.LoginController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class MembresEvenementController implements Initializable {

    @FXML
    private TableView<userReservation> tableReservation;
    @FXML
    private TableColumn<userReservation, String> nom;
    @FXML
    private TableColumn<userReservation, String> prenom;
    @FXML
    private TableColumn<userReservation,String> DateReservation;
     private Label id;
public int idd ;
    @FXML
    private Label nbrPlace;
    @FXML
    private Label nbrMembre;

    public void setNbrPlace(String nbrPlace) {
        this.nbrPlace.setText(nbrPlace);
    }

    public void setNbrMembre(String nbrMembre) {
        this.nbrMembre.setText(nbrMembre);
    }
    
 public void setIdd(int idd) {
        this.idd = idd;
    }
    public void setId(String id) {
        this.id.setText(id);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadMembres();
    }   
    
     private void loadMembres() {
         int ee=idd;
         
       CrudReservation cr=new CrudReservation();
       List<Reservation> lr =cr.afficheMembre(AfficheDetailEvenementController.idEvent);
       
         System.out.println(idEvent);
          ObservableList<userReservation>   data= FXCollections.observableArrayList();
          for (Reservation r: lr)
          {
              
               user u=cr.infoUser(r.getReservation_user());
              userReservation userReserv=new userReservation();
              userReserv.setNom(u.getNom());
              userReserv.setPrenom(u.getPrenom());
              userReserv.setDate(r.getDate_reservation().toString());
             
              
              data.add(userReserv);
            //  nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
              DateReservation.setCellValueFactory(new PropertyValueFactory<>("date"));
              nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
              prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
                 tableReservation.setItems(null);
       tableReservation.setItems(data);
       
    }
      System.out.println(ee);
    }

    

    @FXML
    private void retourDetailEvenement(ActionEvent event) throws IOException {
        CrudEvent cr =new CrudEvent();
             CrudReservation cr2=new CrudReservation();
              FXMLLoader loader =  new FXMLLoader(getClass().getResource("AfficheDetailEvenement.fxml"));
        Parent root = loader.load();
            AfficheDetailEvenementController nav=loader.getController();
            List<Reservation>  reservation=new ArrayList<>();
          Evenement evv=cr.detailEvenment(AfficheDetailEvenementController.idEvent);
          nav.setNom(evv.getNom());
            nav.setDateDebut(evv.getDate_debut().toString());
            nav.setDateFin(evv.getDate_fin().toString());
            nav.setDescription(evv.getDescription());
            nav.setLieu(evv.getLieu());
              nav.setNbrPlace(String.valueOf(evv.getNbrPlace()));
              File file = new File("C:\\wamp64\\www\\Maryam\\web\\image_evenement\\"+evv.getImage().toString());

            Image image1 = new Image(file.toURI().toString());
            nav.image.setImage(image1);
    nav.setNomImage(evv.getImage().toString());
            int nbPlace=evv.getNbrPlace()-cr2.nbrReservé(evv.getId_event());
    if(nbPlace==0)
    {
        nav.setNbrPlace("nom,evenement complet");
    }
    else{
         nav.setNbrPlace("Oui");
    }
          reservation =cr2.afficheMembre(evv.getId_event());
             
          if(evv.getEvenement_user()!=LoginController.userc)
          {
         nav.membres.setVisible(false);
         nav.res.setVisible(true);
               nav.modifier.setVisible(false);
            nav.supprimer.setVisible(false);
        }else{
              nav.res.setVisible(false);
               nav.membres.setVisible(true);
                   nav.modifier.setVisible(true);
            nav.supprimer.setVisible(true);
                }
          if(cr2.estReserver(LoginController.userc, evv.getId_event()) ==false)
          {
              
              nav.setRes("Participer");
              if(nbPlace==0)
              {
                  nav.res.setVisible(false);
              }
              
          }else{
              nav.setRes("Annuler Participation");
          }
             tableReservation.getScene().setRoot(root); 
    }
}
    

