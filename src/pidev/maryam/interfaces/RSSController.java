/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.interfaces;

import static pidev.maryam.interfaces.FluxRss.readRSS;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author ASUS
 */

public class RSSController implements Initializable {
private static String sourceCode="";
    @FXML
    private AnchorPane actualiterpane;
    //private ListView<> actualité;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         System.out.println(readRSS("http://static.blog4ever.com/2011/07/503502/rss_articles.xml"));
         //Label lb1=new Label("titre");
         List<String> l=new ArrayList<String>();
         l=readRSS("http://static.blog4ever.com/2011/07/503502/rss_articles.xml");
         
         ListView lv=new ListView();
         ObservableList<String> ltitre=FXCollections.observableArrayList(l);
        /* for(int i=0;i<l.size();i++)
         {
                 ltitre.add(l.get(i));

       
         }*/
      lv.getItems().add(ltitre);
  actualiterpane.getChildren().addAll(lv);         
    }    
     public static List<String> readRSS (String urlAdress) {
   try{
       URL rssUrl =new URL (urlAdress);
       BufferedReader in = new BufferedReader(new InputStreamReader(rssUrl.openStream()));
       List<String> titre=new ArrayList<>();
    //  List<String> link=new ArrayList<>();
       String line;
       while((line = in.readLine())!= null){
           if(line.contains("<titre>")){
               int firstPos = line.indexOf("<titre>");
               String temp = line.substring(firstPos);
               temp = temp.replace("<titre>", "");
             //  temp = temp.replace("</title>", "");
           
               int lastPos = temp.indexOf("</titre>");
               temp = temp.substring(0, lastPos);
               temp=temp.replace("<![CDATA[", "");
               temp=temp.replace("]]>", "");
               titre.add( temp);
               
               
           /*     firstPos = line.indexOf("<link>");
                temp = line.substring(firstPos);
               temp = temp.replace("<link>", "");
               temp = temp.replace("</link>", "");
           
                lastPos = temp.indexOf("<link>");
               temp = temp.substring(0, lastPos);
              
               link.add(temp);*/
           }
       
   }
        in.close();
        return titre;
        
   } catch (MalformedURLException ue){
       System.out.println("Mal formed URL");
   }
   catch (IOException ioe){
       System.out.println("smthing wen rong");
   }
   return null;
   }
   }