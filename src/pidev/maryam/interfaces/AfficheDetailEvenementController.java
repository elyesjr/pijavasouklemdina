/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.interfaces;

import pidev.maryam.entities.Evenement;
import pidev.maryam.entities.Reservation;
import pidev.maryam.services.CrudEvent;
import pidev.maryam.services.CrudReservation;
import pidev.maryam.services.partage;
import com.restfb.types.Message.Share;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javax.swing.JOptionPane;
import pidev.GUI.LoginController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AfficheDetailEvenementController implements Initializable {

    @FXML
    private Label nom;
    @FXML
    private Label description;
    @FXML
    private Label dateDebut;
    @FXML
    private Label dateFin;
    @FXML
    private Label lieu;
    @FXML
    public Button res;
    @FXML
    public Button supprimer;
    @FXML
    public Button modifier;
    @FXML
    private Button retour;
    @FXML
    public Button membres;
    public static int idEvent;
    public static int id;
     @FXML
    public  ImageView image;
    public String nomImage ;
    @FXML
    private Label nbrPlace;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public Label getNbrPlace() {
        return nbrPlace;
    }

    public void setNbrPlace(String nbrPlace) {
        this.nbrPlace.setText(nbrPlace);
    }

    public static int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }
public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }
    public void setRes(String res)
{
    this.res.setText(res);
}
    public void setNom(String nom) {
        this.nom.setText(nom);
    }

    public void setDescription(String description) {
         this.description.setText(description);
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut.setText(dateDebut);
    }

    public void setDateFin(String dateFin) {
        this.dateFin.setText(dateFin);
    }

    public void setLieu(String lieu) {
           this.lieu.setText(lieu);
    }

    @FXML
    private void partage(ActionEvent event) 
         {
       
        System.out.println(nomImage);
        partage p=new partage();
        p.partager(nom.getText().toString(),description.getText().toString(),dateDebut.getText().toString(),dateFin.getText().toString(),nomImage);
      
    }

    @FXML
    private void reservationEvenement(ActionEvent event) {
  CrudReservation cr=new CrudReservation();

          if(res.getText() =="Annuler Participation")
          {
                Reservation r =new Reservation();
              System.out.println(idEvent);
             // System.out.println(getIdEvenement());
             res.setText("Participer");
           cr.suppAreservation(LoginController.userc, idEvent);
          }else{
                Reservation r =new Reservation();
             //   System.out.println(idEvenement);
               // System.out.println(getIdEvenement());
                r.setEvenement_reservation(idEvent);
             r.setReservation_user(LoginController.userc);
             cr.ajouterReservation(r);
              res.setText("Annuler Participation");
          }
          
    }

    @FXML
    private void supprimerEvenement(ActionEvent event) throws IOException {
        CrudEvent croud=new CrudEvent();
          Evenement evv=croud.detailEvenment(idEvent);
          if(evv.getDate_debut().before(Date.valueOf(LocalDate.now().plusDays(2))))
             JOptionPane.showMessageDialog(null,"Tu ne peut pas supprimer une evenement en cours ou avant 24h de son date dèbut",""
                + "Erreur",2);  
          else{
        croud.DeleteEvent(idEvent);
           FXMLLoader loader =  new FXMLLoader(getClass().getResource("ListEvenement.fxml"));
            Parent root = loader.load();
            
             nom.getScene().setRoot(root); 
          }
    }

    @FXML
    private void modifierEvenement(ActionEvent event) {
        try{
              CrudEvent croud=new CrudEvent();
          Evenement evv=croud.detailEvenment(idEvent);
      if(evv.getDate_debut().before(Date.valueOf(LocalDate.now().plusDays(2))))
             JOptionPane.showMessageDialog(null,"Tu ne peut pas modifier une evenement en cours ou avant 24h de son date dèbut",""
                + "Erreur",2);  
          else{
        FXMLLoader loader =  new FXMLLoader(getClass().getResource("ModifierEvenement.fxml"));
        Parent root = loader.load();
            ModifierEvenementController modif=loader.getController();
            modif.setNbrPlace(String.valueOf( evv.getNbrPlace()));
            modif.setNom(nom.getText());
            modif.setDescription(description.getText());
            modif.setLieu(lieu.getText());
            modif.setIdev(idEvent);
            modif.setFileName(nomImage);
         
             String dd=dateDebut.getText();
       String df=dateFin.getText();
             	LocalDate ldd = LocalDate.parse(dd );
          	LocalDate ldf = LocalDate.parse(df );
     
           modif.getDateDebut().setValue(ldd);
  modif.getDateFin().setValue(ldf);
            
             nom.getScene().setRoot(root);
             modif.image.setImage(image.getImage());
      }
     } catch (IOException ex) {
            Logger.getLogger(ModifierEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }
   
}
    @FXML
    private void retourListEvenement(ActionEvent event) 
        throws IOException {
            FXMLLoader loader =  new FXMLLoader(getClass().getResource("ListEvenement.fxml"));
            Parent root = loader.load();
             nom.getScene().setRoot(root); 
             
    }

    @FXML
    private void afficherMembresEvenement(ActionEvent event) {
         CrudEvent cr =new CrudEvent();
         CrudReservation cr2=new CrudReservation();
        try{
         
        FXMLLoader loader1 =  new FXMLLoader(getClass().getResource("MembresEvenement.fxml"));
        Parent root1 = loader1.load();
            MembresEvenementController membre=loader1.getController();
            Evenement evv=cr.detailEvenment(idEvent);
                 int nbPlace=evv.getNbrPlace()-cr2.nbrReservé(evv.getId_event());
               membre.setIdd(idEvent);
               membre.setNbrMembre(String.valueOf(cr2.nbrReservé(evv.getId_event())));
               membre.setNbrPlace(String.valueOf(nbPlace));
       //     System.out.println(idEvenement);
           description.getScene().setRoot(root1); 
          
       } catch (IOException ex) {
            Logger.getLogger(MembresEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    }
    

