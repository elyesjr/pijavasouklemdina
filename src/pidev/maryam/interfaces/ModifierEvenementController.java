/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.interfaces;

import pidev.maryam.entities.Evenement;
import pidev.maryam.entities.Reservation;
import pidev.maryam.services.CrudEvent;
import pidev.maryam.services.CrudReservation;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import pidev.GUI.LoginController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ModifierEvenementController implements Initializable {

    @FXML
    private TextField nom;
    @FXML
    private TextArea description;
    @FXML
    private TextField lieu;
    @FXML
    private DatePicker DateDebut;
    @FXML
    private DatePicker DateFin;
    @FXML
    private Button modifier;
    @FXML
    public ImageView image;
public String fileName ;
    public int idev ;
    @FXML
    private Button retour;
    @FXML
    private TextField nbrPlace;

    public void setNbrPlace(String nbrPlace) {
        this.nbrPlace.setText(nbrPlace);
    }
    
     public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setIdev(int idev) {
        this.idev = idev;
    }

 public void setNom(String nom) {
        this.nom.setText(nom);
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    public void setLieu(String lieu) {
        this.lieu.setText(lieu);
    }

    public void setDateFin(DatePicker dateFin) {
        this.DateFin.setValue(LocalDate.MIN);
    }

    public void setDateDebut(DatePicker dateDebut) {
        this.DateDebut = dateDebut;
    }
    public DatePicker getDateDebut() {
        return DateDebut;
    }

    public DatePicker getDateFin() {
        return DateFin;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public  String upload(File file) throws  IOException {
     BufferedOutputStream stream = null;
        BufferedOutputStream stream2 = null;
        String globalPath="C:\\wamp64\\www\\Maryam\\web\\image_evenement";
        String globalPath2="C:\\Users\\ASUS\\Documents\\NetBeansProjects\\Pidevjava\\src\\imagesEvenement";
        String localPath="localhost:8080/";
       fileName = file.getName();
        fileName=fileName.replace(" ", "_");
        try {
            Path p = file.toPath();
            
            byte[] bytes = Files.readAllBytes(p);
    
            File dir = new File(globalPath);
            File dir2 = new File(globalPath2);
            if (!dir.exists())
                dir.mkdirs();
            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()+File.separator + fileName);
            File serverFile2 = new File(dir2.getAbsolutePath()+File.separator + fileName);
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream2 = new BufferedOutputStream(new FileOutputStream(serverFile2));
            stream.write(bytes);
            stream.close();
            stream2.write(bytes);
            stream2.close();
            return localPath+"/"+fileName;
        } catch (IOException ex) {
            Logger.getLogger(AjoutEventFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            return "error2";
        
        }
      }
    @FXML
    private void choosefile(ActionEvent event) throws IOException {
         FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("file", "*.jpg", "*.png"));
		File selectedfile = fileChooser.showOpenDialog(null);
		if (selectedfile != null) {
			
			upload(selectedfile);
                        System.out.println("    "+upload(selectedfile));
			Image image = new Image(new FileInputStream(selectedfile));
			this.image.setImage(image);
		}
    }
    @FXML
    private void modifierEvenement(ActionEvent event) throws ParseException, IOException {
        
         if(nom.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ nom",""
                + "Erreur",2);  
         }else {if(description.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ description",""
                + "Erreur",2);  
         }else{ if(lieu.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ emplacement",""
                + "Erreur",2);  
         }else {if(DateDebut.getValue()==null)
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ date debut",""
                + "Erreur",2);  
         }else {if(DateFin.getValue()==null)
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ date fin",""
                + "Erreur",2);  
         }
      
         else{ 
              String DATE_PATTERN = "yyyy/MM/dd";
          
        DateTimeFormatter DATE_FORMATTER
                = DateTimeFormatter.ofPattern(DATE_PATTERN);
        String date1 = DATE_FORMATTER.format(DateDebut.getValue());
         String date2 = DATE_FORMATTER.format(DateFin.getValue());

       
        
       
        java.util.Date utilDate=new java.util.Date();
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy/MM/dd");
        utilDate=formatter.parse(date1);
        java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
        
         
        java.util.Date utilDate2=new java.util.Date();
        SimpleDateFormat formatter2=new SimpleDateFormat("yyyy/MM/dd");
        utilDate=formatter.parse(date2);
        java.sql.Date sqlDate2=new java.sql.Date(utilDate.getTime());
         if(sqlDate.after(sqlDate2))
         
         {
           JOptionPane.showMessageDialog(null,"il faut choisissez la date debut avant la date fin",""
                + "Erreur",2);  
         }
             else{ if(sqlDate2.before(Date.valueOf(LocalDate.now())))
         
         {
           JOptionPane.showMessageDialog(null,"il faut choisissez la date fin après la date actuel",""
                + "Erreur",2);  
         }    else{ if(sqlDate.before(Date.valueOf(LocalDate.now())))
         
         {
           JOptionPane.showMessageDialog(null,"il faut choisissez la date debut après la date actuel",""
                + "Erreur",2);  
         }  else {if(fileName==null)
         
         {
           JOptionPane.showMessageDialog(null,"il faut choisissez une image",""
                + "Erreur",2);  
         }
          else{
     
        
        
        
        CrudEvent crud=new CrudEvent();
        Evenement ev=new Evenement();
        ev.setNom(nom.getText().toString());
        ev.setDescription(description.getText().toString());
        ev.setDate_debut(sqlDate);
        ev.setDate_fin(sqlDate2);
        ev.setLieu(lieu.getText().toString());
        ev.setImage(fileName);
        ev.setNbrPlace(Integer.valueOf(nbrPlace.getText()));
        System.out.println(fileName);
        System.out.println(idev);
        crud.Update(ev, idev);
        
         FXMLLoader loader =  new FXMLLoader(getClass().getResource("ListEvenement.fxml"));
            Parent root = loader.load();
              nom.getScene().setRoot(root); 
         }}}}}}}}}
    }

    @FXML
    private void retourDetailEvenement(ActionEvent event) throws IOException {
         CrudEvent cr =new CrudEvent();
             CrudReservation cr2=new CrudReservation();
              FXMLLoader loader =  new FXMLLoader(getClass().getResource("AfficheDetailEvenement.fxml"));
        Parent root = loader.load();
            AfficheDetailEvenementController nav=loader.getController();
            List<Reservation>  reservation=new ArrayList<>();
          Evenement evv=cr.detailEvenment(AfficheDetailEvenementController.idEvent);
          nav.setNom(evv.getNom());
            nav.setDateDebut(evv.getDate_debut().toString());
            nav.setDateFin(evv.getDate_fin().toString());
            nav.setDescription(evv.getDescription());
            nav.setLieu(evv.getLieu());
              nav.setNbrPlace(String.valueOf(evv.getNbrPlace()));
            File file = new File("C:\\wamp64\\www\\Maryam\\web\\image_evenement"+evv.getImage().toString());

            Image image1 = new Image(file.toURI().toString());
            nav.image.setImage(image1);
    int nbPlace=evv.getNbrPlace()-cr2.nbrReservé(evv.getId_event());
    if(nbPlace==0)
    {
        nav.setNbrPlace("nom,evenement complet");
    }
    else{
         nav.setNbrPlace("Oui");
    }
          reservation =cr2.afficheMembre(evv.getId_event());
             
          if(evv.getEvenement_user()!=LoginController.userc)
          {
         nav.membres.setVisible(false);
         nav.res.setVisible(true);
               nav.modifier.setVisible(false);
            nav.supprimer.setVisible(false);
        }else{
              nav.res.setVisible(false);
               nav.membres.setVisible(true);
                   nav.modifier.setVisible(true);
            nav.supprimer.setVisible(true);
                }
          if(cr2.estReserver(LoginController.userc, evv.getId_event()) ==false)
          {
              
              nav.setRes("Participer");
              if(nbPlace==0)
              {
                  nav.res.setVisible(false);
              }
              
          }else{
              nav.setRes("Annuler Participation");
          }
             nom.getScene().setRoot(root); 
    }


  


 

    
}

