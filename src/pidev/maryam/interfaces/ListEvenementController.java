/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.interfaces;

import pidev.maryam.entities.Evenement;
import pidev.maryam.entities.Reservation;
import pidev.maryam.services.CrudEvent;
import pidev.maryam.services.CrudReservation;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import pidev.GUI.LoginController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ListEvenementController implements Initializable {
    @FXML
    private TableView<Evenement> table;

    @FXML
    private TableColumn<Evenement, String> nom;
    @FXML
    private TableColumn<Evenement, Date> datedebut;
    @FXML
    private TableColumn<Evenement, Date> datefin;
    @FXML
    private Button details;
     @FXML
    private Button retour;
    @FXML
    private TextField recherche;
// private ObservableList <Evennement> data ;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
          LoadEvenement();
          recherche();
    }    

   public void LoadEvenement()
    {
        CrudEvent cr =new CrudEvent();
        List<Evenement> l=cr.listEvenement();
     ObservableList<Evenement>   data= FXCollections.observableArrayList();
     for (Evenement e : l)
     {    

         if(e.getDate_fin().before(Date.valueOf(LocalDate.now())))
         {
              cr.DeleteEvent(e.getId_event());

         }
     }
       List<Evenement> l2=cr.listEvenement();
         for (Evenement e : l2)
     {
         data.add(e);
         System.out.println(e.getId_event());
     
     nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
      datedebut.setCellValueFactory(new PropertyValueFactory<>("date_debut"));
       datefin.setCellValueFactory(new PropertyValueFactory<>("date_fin"));
       table.setItems(null);
       table.setItems(data);
     }
    
}

   @FXML
    private void afficheDetailEvenement(ActionEvent event)  {
         try {
           CrudEvent cr =new CrudEvent();
             CrudReservation cr2=new CrudReservation();
        ObservableList<Evenement> selectedRows, Ev;
        Ev= table.getItems();
        selectedRows = table.getSelectionModel().getSelectedItems();
        //Main.SelectedID= selectedRows.get(0);
        
        FXMLLoader loader =  new FXMLLoader(getClass().getResource("AfficheDetailEvenement.fxml"));
        Parent root = loader.load();
            AfficheDetailEvenementController nav=loader.getController();
              if(!selectedRows.isEmpty())
            {
        for(Evenement e :selectedRows)
        {
         
            int id=e.getId_event();
             nav.setIdEvent(id);
            Evenement evv=cr.detailEvenment(id);
            nav.setNom(evv.getNom());
            nav.setDateDebut(evv.getDate_debut().toString());
            nav.setDateFin(evv.getDate_fin().toString());
            nav.setDescription(evv.getDescription());
            nav.setLieu(evv.getLieu());
          //  System.out.println(evv.getNbrPlace());
       
            File file = new File("C:\\wamp64\\www\\Maryam\\web\\image_evenement\\"+evv.getImage().toString());

            Image image1 = new Image(file.toURI().toString());
            nav.image.setImage(image1);
    nav.setNomImage(evv.getImage().toString());
            int nbPlace=evv.getNbrPlace()-cr2.nbrReservé(evv.getId_event());
    if(nbPlace==0)
    {
        nav.setNbrPlace("nom,evenement complet");
    }
    else{
         nav.setNbrPlace("Oui");
    }
         
          if(e.getEvenement_user()!=LoginController.userc)
          {
         nav.membres.setVisible(false);
         nav.res.setVisible(true);
  
               nav.modifier.setVisible(false);
            nav.supprimer.setVisible(false);
        }else{
              nav.res.setVisible(false);
               nav.membres.setVisible(true);
                   nav.modifier.setVisible(true);
            nav.supprimer.setVisible(true);
                }
                    if(cr2.estReserver(LoginController.userc, id) ==false)
          {
              
              nav.setRes("Participer");
              if(nbPlace==0)
              {
                  nav.res.setVisible(false);
              }
              
          }else{
              nav.setRes("Annuler Participation");
          }
        
        }
        
               table.getScene().setRoot(root);
            }
             } catch (IOException ex) {
            Logger.getLogger(AfficheDetailEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void recherche() {
        
        
           ObservableList<Evenement>   data= FXCollections.observableArrayList();
           CrudEvent cr=new CrudEvent();
        recherche.setOnKeyReleased(e -> {
            if (recherche.getText().equals("")) {
               LoadEvenement();
                System.out.println("fer8a");
            } else {
               table.setItems(null);
                data.clear();
               List<Evenement> l2=cr.rechercheEvenement(recherche.getText().toString());
               System.out.println(recherche.getText().toString());
                 //  data= FXCollections.observableArrayList();
//     for (Evenement ev : l)
//     {    
//
//         if(ev.getDate_fin().before(Date.valueOf(LocalDate.now())))
//         {
//              cr.DeleteEvent(ev.getId_event());
//
//         }
//     }
      // List<Evenement> l2=cr.rechercheEvenement(recherche.getText().toString());
         for (Evenement ev : l2)
     {
         data.add(ev);
      //   System.out.println(e.getId_evenement());
     
     nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
      datedebut.setCellValueFactory(new PropertyValueFactory<>("date_debut"));
       datefin.setCellValueFactory(new PropertyValueFactory<>("date_fin"));
      // table.setItems(null);
       table.setItems(data);
     }
            }
    });
    }
    @FXML
    private void retourListEvenement(ActionEvent event) 
        throws IOException {
            FXMLLoader loader =  new FXMLLoader(getClass().getResource("AjoutEventFXML.fxml"));
            Parent root = loader.load();
             retour.getScene().setRoot(root); 
    }}

   
    

