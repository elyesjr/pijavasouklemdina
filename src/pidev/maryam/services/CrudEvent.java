/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.services;

import pidev.maryam.entities.Evenement;
import pidev.maryam.util.MyConnexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CrudEvent {
    Statement st;
    PreparedStatement pst;
    ResultSet res;
    Connection cn = MyConnexion.getInstance().getConnection();
    
     public void ajouterEvent(Evenement e){
     
            String requete = "INSERT INTO `evennement`(`evenement_user`,`nom`, `date_debut`, `date_fin`, `image`, `lieu`, `description`, `etat`,`nbrPlace`)"
                    + " VALUES (?,?,?,?,?,?,?,?,?)";
                try {     
            PreparedStatement st=cn.prepareStatement(requete);
            st.setInt(1, e.getEvenement_user());
            st.setString(2, e.getNom());
            st.setDate(3,  e.getDate_debut());
            st.setDate(4,  e.getDate_fin());
            st.setString(5, e.getImage());
            st.setString(6, e.getLieu());
            st.setString(7, e.getDescription());
            st.setInt(9, e.getNbrPlace());
            st.setInt(8, 0);
            
            //exécution la requet
            st.executeUpdate();
            System.out.println("Evénement ajouter");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
}
      public List<Evenement> listEvenement(){
         List<Evenement> myList = new ArrayList<Evenement>();
         String requete="SELECT * FROM `evennement` WHERE etat=0 ORDER BY date_debut";
         try{
        
         Statement st2=cn.createStatement();
         ResultSet rs =st2.executeQuery(requete);
         while(rs.next()){
             Evenement e =new Evenement();
             e.setId_event(rs.getInt(1));
             e.setEvenement_user(rs.getInt(2));
             e.setNom(rs.getString(3));
             e.setDate_debut(rs.getDate(4));
             e.setDate_fin(rs.getDate(5));
             e.setImage(rs.getString(6));
             e.setLieu(rs.getString(7));
              e.setDescription(rs.getString(8));
               e.setEtat(rs.getInt(9));
             myList.add(e);
         }
         }
          catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
         return myList;
}
      
       public void Update(Evenement e,int id) 
               {
        String requete = "UPDATE `evennement` SET `nom`=?,"
                + "`date_debut`=?,`date_fin`=?,`image`=?,`lieu`=?,"
                + "`description`=?,`etat`=?,`nbrPlace`=? WHERE  id_event = ?;";
        try{
         PreparedStatement pst=cn.prepareStatement(requete);

            pst.setString(1, e.getNom());
            pst.setDate(2, (Date) e.getDate_debut());
            pst.setDate(3, (Date) e.getDate_fin());
            pst.setString(4, e.getImage());
            pst.setString(5, e.getLieu());
            pst.setString(6, e.getDescription());
            pst.setInt(7, e.getEtat());
 
            pst.setInt(8, e.getNbrPlace());
                   pst.setInt(9, id);
        pst.executeUpdate();
            System.out.println("evenement modifier");
          } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
    }
       
        public void DeleteEvent(int id)
                {
        String requete = "UPDATE `evennement` SET `etat`=1 WHERE `id_event` =" +id ;
        try{
       PreparedStatement st2=cn.prepareStatement(requete);
        st2.executeUpdate();
        System.out.println("evenement supprimer");
          } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
        
        public List<Evenement> rechercheEvenement(String nom)
    {
        List<Evenement> list =new ArrayList<>();
        String requete="SELECT * FROM `evennement` WHERE nom LIKE  '%"+ nom +"%'"
                + "  and etat=0 order by date_debut";
         try{
            Statement st2=cn.createStatement();
         ResultSet rs =st2.executeQuery(requete);
         while(rs.next()){
                 Evenement e = new Evenement();
                     e.setId_event(rs.getInt(1));
             e.setEvenement_user(rs.getInt(2));
             e.setNom(rs.getString(3));
             e.setDate_debut(rs.getDate(4));
             e.setDate_fin(rs.getDate(5));
             e.setImage(rs.getString(6));
             e.setLieu(rs.getString(7));
              e.setDescription(rs.getString(8));
               e.setEtat(rs.getInt(9));
             list.add(e);
             }
         }catch(SQLException ex){
      ex.printStackTrace();
  }
                 
        return list;
    }
        
    public Evenement detailEvenment(int id)
    {
         Evenement e = new Evenement();
        String requete="SELECT * FROM `evennement` WHERE `id_event` =" +id;
          try{
             Statement st2=cn.createStatement();
         ResultSet rs =st2.executeQuery(requete);
         while(rs.next()){
                
                 
                     e.setId_event(rs.getInt(1));
             e.setEvenement_user(rs.getInt(2));
             e.setNom(rs.getString(3));
             e.setDate_debut(rs.getDate(4));
             e.setDate_fin(rs.getDate(5));
             e.setImage(rs.getString(6));
             e.setLieu(rs.getString(7));
              e.setDescription(rs.getString(8));
               e.setEtat(rs.getInt(9));
               e.setNbrPlace(rs.getInt(10));
             
             }
             }catch(SQLException ex){
      ex.printStackTrace();
    }
    return e ;      
}
}