/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.maryam.services;

import pidev.maryam.entities.Reservation;
import pidev.maryam.entities.user;
import pidev.maryam.util.MyConnexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CrudReservation {
    Statement st;
    PreparedStatement pst;
    ResultSet res;
     Connection cn = MyConnexion.getInstance().getConnection();
    public void ajouterReservation(Reservation res)
    {
     
           String requete="INSERT INTO `reservation`(`reservation_user`, `evenement_reservation`, `date_reservation`) VALUES (?,?,?)";
           try{
        PreparedStatement st =cn.prepareStatement(requete);
        
        st.setInt(1, res.getReservation_user());
        st.setInt(2, res.getEvenement_reservation());
        st.setDate(3, Date.valueOf(LocalDate.now()));
        st.executeUpdate();
            System.out.println("reservation ajouter");
        }catch(SQLException ex)
       {
           ex.printStackTrace();
       }
    }
     
     public void suppAreservation(int idUser, int idEvent)
     {
         String requete="DELETE FROM `reservation` WHERE reservation_user=? and evenement_reservation=?";
         try{
             PreparedStatement st=cn.prepareStatement(requete);
             st.setInt(1, idUser);
             st.setInt(1, idEvent);
               st.executeUpdate();
            System.out.println("participation annuler");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
             
             
         }
     
    public List<Reservation> afficheMembre(int idEv)
    {
        List<Reservation> list=new ArrayList<Reservation>();
        String requete="SELECT * FROM reservation WHERE evenement_reservation="+idEv;
         try{ 
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(requete);
            while (rs.next())
            {
                Reservation r=new Reservation();
                //r.set(rs.getDate(4));
                r.setReservation_user(rs.getInt(2));
                
                r.setDate_reservation(rs.getDate(4));
                list.add(r);
            }
        }catch(SQLException ex)
             {
           ex.printStackTrace();
       }
        return list;
    }
    
    
         public boolean estReserver(int idu ,int ide)
    {
          List<Reservation> l=new ArrayList<Reservation>();
       boolean existe=false  ;
       String requete="SELECT * FROM `reservation` WHERE evenement_reservation= "+ide+" and reservation_user= "+idu;
    
try{ 
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(requete);
            while (rs.next())
            {
                Reservation r=new Reservation();
                r.setDate_reservation(rs.getDate(4));
                l.add(r);
            }
          
}catch(SQLException ex)
       {
           ex.printStackTrace();
       }
 if(l.isEmpty()==true)
     return false ;
 else return true ;
}
    
         public  int nbrReservé(int id){
             int count=0;
             String requete="SELECT COUNT(*) FROM reservation WHERE evenement_reservation="+id;
         try{ 
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(requete);
            while (rs.next())
            {
              count=rs.getInt(1);
              
            }
        }catch(SQLException ex)
             {
           ex.printStackTrace();
       }
        return count;
             
         }
         public user infoUser(int id)
         {
                  user u=new user();
            String requete="SELECT * FROM `fos_user` WHERE id="+id;
             try{ 
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(requete);
            while (rs.next())
            {
           
                u.setNom(rs.getString(14));
                u.setPrenom(rs.getString(15));
                 }
        }catch(SQLException ex)
             {
           ex.printStackTrace();
       }
             return u ;
         }
     }
     

