/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.mindrot.jbcrypt.BCrypt;
import pidev.entities.boutique;
import pidev.entities.user;
import pidev.services.UserService;
import pidev.utils.myconnexionpi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class LoginController implements Initializable {

    @FXML
    private Button btnlogin;
    @FXML
    private TextField txtlogin;
    @FXML
    private PasswordField txtpass;
Connection cn = myconnexionpi.getInstance().getConnection();
PreparedStatement st = null;
ResultSet resultSet = null;


Stage dialogStage = new Stage();
Scene scene;
    @FXML
    private Hyperlink idsins;
 
 public static int userc;
  public static String username;

 
    public static int getUserc() {
        
        
        return userc;
        
        
        // TODO
    }    

    /**
     * Initializes the controller class.
     */
    public static void setUserc(int userc) { 
        LoginController.userc = userc;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
        
    @FXML
    private void Login(ActionEvent event) throws SQLException {
        String login = txtlogin.getText();
        String password = txtpass.getText();
        String passwordBD = "";
        String sql = "SELECT * FROM fos_user WHERE username = ?";
       // String sql_2 = "SELECT * FROM fos_user WHERE username = ? and password = ?";
        
        try{
       st = cn.prepareStatement(sql);
       st.setString(1,login);
       resultSet = st.executeQuery();
       
       if(resultSet.last())
        passwordBD = resultSet.getString("password");
       
       
        boolean passMatch = UserService.checkPassword(password, passwordBD);    
       
       if(passMatch)

       {
          userc=resultSet.getInt(1);
          username=resultSet.getString(2);
          infoBox("Login Successfull", "Success", null);
          
                    user user = UserService.getUser(userc);
                    System.out.println(user.getRoles());
                    
                    if(user.getRoles().contains("ROLE_ADMIN")){
                        Stage stage = new Stage();
                        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("pidev/GUI/ajoutartisan.fxml"));
                        Scene scene = new Scene(root);
                        stage = (Stage) btnlogin.getScene().getWindow();
                        stage.close();
                        stage.setScene(scene);
                        stage.show();
                    }
                    else{
                        Stage stage = new Stage();
                        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("pidev/GUI/Ajoutboutique.fxml"));
                        Scene scene = new Scene(root);
                        stage = (Stage) btnlogin.getScene().getWindow();
                        stage.close();
                        stage.setScene(scene);
                        stage.show();
                    }
//       Node source = (Node) event.getSource();
//       dialogStage = (Stage) source.getScene().getWindow();
//       dialogStage.close();
//       scene = new Scene(FXMLLoader.load(getClass().getResource("Ajoutboutique.fxml")));
//       dialogStage.setScene(scene);
//       dialogStage.show();
       }
       else{
        infoBox("Enter Correct Login and Password", "Failed", null);
               }
       }catch(Exception e){
       e.printStackTrace();
       }
       }

       public static void infoBox(String infoMessage, String titleBar, String headerMessage)
       {
       Alert alert = new Alert(AlertType.INFORMATION);
       alert.setTitle(titleBar);
       alert.setHeaderText(headerMessage);
       alert.setContentText(infoMessage);
       alert.showAndWait();
       }
           @FXML    
           private void goToRegisterAction(MouseEvent event) {

               try {
                   Stage stage = new Stage();
                   Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("pidev/GUI/register.fxml"));
                   Scene scene = new Scene(root);
                   stage = (Stage) idsins.getScene().getWindow();
                   stage.close();
                   stage.setScene(scene);
                   stage.show();

               } catch (IOException ex) {
                   System.err.println(ex.getMessage());
               }   
               }

       }


