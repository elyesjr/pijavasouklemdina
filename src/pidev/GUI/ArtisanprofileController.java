/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class ArtisanprofileController implements Initializable {

    @FXML
    private Label nom;
    @FXML
    private Label adresse;
    @FXML
    private Label tel;
    @FXML
    private Button idret;
    @FXML
    private ImageView idimage;
     public int idart ;
    public ImageView getIdimage() {
        return idimage;
    }

    public void setIdimage(Image idimage) {
        this.idimage.setImage(idimage);
    }
   public void setNom(String nom) {
        this.nom.setText(nom);
    }

    public void setAdresse(String adresse) {
        this.adresse.setText(adresse);
    }

    public void setTel(String tel) {
        this.tel.setText(tel);
    }

    public void setIdart(int idart) {
        this.idart = idart;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void retourenarriere(ActionEvent event) throws IOException {
          FXMLLoader loader =new FXMLLoader(getClass().getResource("listeartisan.fxml"));
        Parent root=loader.load();
         idret.getScene().setRoot(root);
    }
    
}
