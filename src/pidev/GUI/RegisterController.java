/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import pidev.entities.user;
import pidev.services.UserService;
import pidev.utils.myconnexionpi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class RegisterController implements Initializable {

    @FXML
    private AnchorPane rootpane;
    @FXML
    private TextField email;
    @FXML
    private TextField username;
    @FXML
    private TextField nom;
    @FXML
    private TextField adresse;
    @FXML
    private TextField prenom;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField rpassword;
    @FXML
    private Button registerButton;
    @FXML
    private DatePicker birthday;
    @FXML
    private ComboBox<String> roles;
    @FXML
    private Button loginButton;
        Connection cn = myconnexionpi.getInstance().getConnection();
        String Admin = "ADMIN";
    String Client = "CLIENT";
    String Vendeur = "VENDEUR";

    /**
     * Initializes the controller class.
     */
    

    @FXML
    private void goToLoginAction(ActionEvent event) {
        
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("pidev/GUI/Login.fxml"));
            Scene scene = new Scene(root);
            stage = (Stage) loginButton.getScene().getWindow();
            stage.close();
            stage.setScene(scene);
            stage.show();
            
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }   
    }

    @FXML
    private void resgiterAction(ActionEvent event) throws SQLException, FileNotFoundException, IOException {
                    if(email.getText().equals(""))
         
         {
             JOptionPane.showMessageDialog(null,"il faut remplir le champ mail",""
                + "Erreur",2);  
         }else{
            
             if (username.getText().equals("")){
               JOptionPane.showMessageDialog(null,"il faut remplir le champ username",""
                + "Erreur",2);       }
            else{  if (nom.getText().equals("")) {
                JOptionPane.showMessageDialog(null,"il faut remplir le champ nom",""
                + "Erreur",2); 
            }else{
             if (prenom.getText().equals("")) {
                JOptionPane.showMessageDialog(null,"il faut remplir le champ prenom",""
                + "Erreur",2); 
            }else{
                 if (password.getText().equals("")) {
                JOptionPane.showMessageDialog(null,"il faut remplir le champ password",""
                + "Erreur",2); 
            }else{
                     if (adresse.getText().equals("")) {
                JOptionPane.showMessageDialog(null,"il faut remplir le champ adresse",""
                + "Erreur",2); 
            }else{     if (rpassword.getText().equals("")) {
                JOptionPane.showMessageDialog(null,"il faut remplir le champ password",""
                + "Erreur",2); 
            }else{
                         
                     
                 
        String recupererRole = roles.getValue();
        String role = "";
        if(recupererRole.contains("ADMIN")){
            role = "a:1:{i:0;s:10:\"ROLE_ADMIN\";}";
        }
        if(recupererRole.contains("CLIENT")){
           
            role =  "a:1:{i:0;s:11:\"ROLE_CLIENT\";}";
        }
        if(recupererRole.contains("VENDEUR")){
           
            role = "a:1:{i:0;s:12:\"ROLE_VENDEUR\";}";
        }
        
        
        user user = new user();
        UserService uss = new UserService();
        boolean verif;
        
        if
                (uss.verifierUsername(username.getText()) == 0 && uss.verifierEmail(email.getText()) == 0 && email.getText().matches("^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\\.[a-z]{1,4}$")){
            

            
            String requete = "INSERT INTO fos_user (username,username_canonical,email,email_canonical,enabled,password,roles,nom,prenom,datedenaissance,adresse) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement st = cn.prepareStatement(requete);

            st.setString(1, username.getText());
            st.setString(2, username.getText());
            st.setString(3, email.getText());
            st.setString(4, email.getText());
            st.setString(5, "1");
            st.setString(6, UserService.hashPassword(password.getText()));
            st.setString(7, role);
            st.setString(8, nom.getText());
            st.setString(9, prenom.getText());
            st.setString(10, String.valueOf(birthday.getValue()));
            st.setString(11, adresse.getText());
            String path = null;
        
//            File file = new File(path);
//            FileInputStream fis = new FileInputStream(file);
//            st.setBinaryStream(12, fis, (int)file.length());
            
            st.executeUpdate();
            
            System.out.println("Compte Creer");
            
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("pidev/GUI/Login.fxml"));
            Scene scene = new Scene(root);
            stage = (Stage) loginButton.getScene().getWindow();
            stage.close();
            stage.setScene(scene);
            stage.show();
            
        }
            }}}}}}}
            
//        System.err.println("email incorrect");
        
        
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         ObservableList<String> options = FXCollections.observableArrayList(
              Admin,
                Client,
                Vendeur
        );
        roles.setItems(options);
        // TODO
    }    

    @FXML
    private void colorUsername(KeyEvent event) throws SQLException {
           UserService ss = new UserService();
        if( ss.verifierUsername(username.getText()) == 1 ){
            //when it not matches the pattern (1.0 - 6.0)
            //set the textField empty
            username.setStyle("-fx-background-color: red");
        }
        else{
            username.setStyle("-fx-background-color: green");
        }
    }

    @FXML
    private void checkPass(KeyEvent event) {
         if( (password.getText() == null ? rpassword.getText() == null : password.getText().equals(rpassword.getText())) ){
         rpassword.setStyle("-fx-background-color: green");
        }
        else{
         rpassword.setStyle("-fx-background-color: red");
        }
    }

    @FXML
    private void colorEmail(KeyEvent event) throws SQLException {
         System.out.println("12");
        UserService ss = new UserService();
        System.out.println(ss.verifierEmail(email.getText()));
        if( ss.verifierEmail(email.getText()) == 1 || !email.getText().matches("^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\\.[a-z]{1,4}$") ){
            email.setStyle("-fx-background-color: red");
        }
        else{
            email.setStyle("-fx-background-color: green");
        }
        
    }
    
}
