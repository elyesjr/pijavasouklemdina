/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import static org.omg.CORBA.AnySeqHelper.id;
import static pidev.GUI.LoginController.userc;
import pidev.entities.artisan;
import pidev.entities.boutique;
import pidev.services.myservicespi;
import pidev.utils.myconnexionpi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class ListeartisanController implements Initializable {

    @FXML
    private Button listartid;
    @FXML
    private TableView<artisan> tableart;
   
    @FXML
    private Button updateartbtn;
    @FXML
    private Button deleteartbtn;
    @FXML
    private TextField txtnumtelart;
    @FXML
    private TextField txtadrart;
    @FXML
    private TextField txtnomart;
    @FXML
    private Button btnartimg;
    @FXML
    private ImageView imageart;
    String fileName;
    @FXML
    private TableColumn<artisan, String> idnomart1;
    @FXML
    private TableColumn<artisan, String> idadrart1;
    @FXML
    private TableColumn<artisan, String> idnumtelart;
    @FXML
    private TableColumn<artisan, String> idimart;
    @FXML
    private TextField rechercher;
    @FXML
    private Button idrech;
    @FXML
    private Button idret;
    @FXML
    private Button ifaffiche;
    @FXML
    private ImageView iddel;
    @FXML
    private ImageView idupd;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       txtnomart.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(oldValue){
                    if (txtnomart.getText().length() == 8)
                        txtnomart.setStyle("-fx-background-color: green");
                    else
                        txtnomart.setStyle("-fx-background-color: red");
                }
            }
            
        });
        setCellValueFromTableToTextFieled();
        artisan a=new artisan();
        // TODO
    }    

    @FXML
    private void listeart(ActionEvent event) throws SQLException {
      
        Connection cn = myconnexionpi.getInstance().getConnection();
   myservicespi croud=new myservicespi();
     List<artisan> la=croud.listerartisans();
        ObservableList<artisan> data = FXCollections.observableArrayList();
      
           //    ResultSet rs = cn.createStatement().executeQuery("SELECT * FROM `boutique`");
    for(artisan e:la)
    {
        data.add(e);
        
        idnomart1.setCellValueFactory(new PropertyValueFactory<>("nomart"));
          idnumtelart.setCellValueFactory(new PropertyValueFactory<>("adresse"));
          idadrart1.setCellValueFactory(new PropertyValueFactory<>("numtel")); 
          idimart.setCellValueFactory(new PropertyValueFactory<>("image"));
         
     
      
          tableart.setItems(null);  
        tableart.setItems(data);   
    }        
        
        
        
        
        
//        ObservableList data = FXCollections.observableArrayList();
//               ResultSet rs = cn.createStatement().executeQuery("SELECT * FROM `artisan`");
//        try {          
//            while (rs.next()) {           
//                data.add(new artisan(rs.getString(4),rs.getString(3), rs.getString(5), rs.getString(6)));     
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(ListeartisanController.class.getName()).log(Level.SEVERE, null, ex);           
//        }
//        idnomart1.setCellValueFactory(new PropertyValueFactory<>("nomart"));
//        
//        idadrart1.setCellValueFactory(new PropertyValueFactory<>("adresse"));
//                idnumtelart.setCellValueFactory(new PropertyValueFactory<>("numtel"));
//
//        idimart.setCellValueFactory(new PropertyValueFactory<>("image"));
//          tableart.setItems(null);  
//        tableart.setItems(data);
    }

    @FXML
    private void updateart(ActionEvent event) throws SQLException, IOException {
Connection cn= myconnexionpi.getInstance().getConnection();
       myservicespi crud=new myservicespi();
        
    ObservableList<artisan> selectedRows, arti;
        arti= tableart.getItems();
        selectedRows = tableart.getSelectionModel().getSelectedItems();
           
           
        for(artisan a:selectedRows)
        {
        artisan aa=new artisan();
           System.out.println(a.getId());
            int id=a.getId();
            a.setNomart(txtnumtelart.getText());
            a.setNumtel(txtnomart.getText());
            a.setAdresse(txtadrart.getText());
             if (fileName==null)
                
            a.setImage(a.getImage());
            else 
                a.setImage(fileName);
             listeart(event);
    File file = null;
          upload(file);
            
            crud.Modifierartisan(a, id);
   
//    artisan a=new artisan(txtadrart.getText(),txtnomart.getText(),txtnumtelart.getText(),fileName);
//        myservicespi sp=new myservicespi();
//        
//   sp.ajouterArtisan(a);
    }}

    @FXML
    private void deleteart(ActionEvent event) throws SQLException, IOException {
     //   Connection cn= myconnexionpi.getInstance().getConnection();
       myservicespi crud=new myservicespi();
        
    ObservableList<artisan> selectedRows, arti;
        arti= tableart.getItems();
        selectedRows = tableart.getSelectionModel().getSelectedItems();
           
           
        for(artisan a:selectedRows)
        {
        artisan aa=new artisan();
           System.out.println(a.getId());
            int id=a.getId();
            listeart(event);
           
            
            
            crud.supprimerartisan(id);
   
    }
    }
    @FXML
    private void chooseimage(ActionEvent event) throws IOException {
          FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("file", "*.jpg", "*.png"));
		File selectedfile = fileChooser.showOpenDialog(null);
		if (selectedfile != null) {
			
			upload(selectedfile);
                        System.out.println("    "+upload(selectedfile));
			Image image1 = new Image(new FileInputStream(selectedfile));
			imageart.setImage(image1);
		}
    }
    
    
     public  String upload(File file) throws  IOException {
            return "error2";

          }
   
    private void setCellValueFromTableToTextFieled(){
        tableart.setOnMouseClicked(e->{
        artisan a2=tableart.getItems().get(tableart.getSelectionModel().getSelectedIndex());
      
       txtnumtelart.setText(a2.getNomart());
       
       
        txtadrart.setText(a2.getNumtel());
                txtnomart.setText(a2.getAdresse());

         
        imageart.setImage(new Image(ClassLoader.getSystemResource("images/"+a2.getImage()).toString()));

        if (LoginController.userc==a2.getUser_id())
                {
                
        updateartbtn.setVisible(true);
                deleteartbtn.setVisible(true);
                iddel.setVisible(true);
        idupd.setVisible(true);

                }  else{ updateartbtn.setVisible(false);
                deleteartbtn.setVisible(false);
                     iddel.setVisible(false);
        idupd.setVisible(false);
               
             }
        
        });

}

    @FXML
    private void recherche(ActionEvent event) {
                  artisan a =new artisan();
        myservicespi sp=new myservicespi();
//   sp.rechercherBoutique(idrech.getText());}

        String search = rechercher.getText();
        
        tableart.setItems((ObservableList<artisan>) sp.rechercherartisan(search));
    }

    @FXML
    private void retourner(ActionEvent event) throws IOException {
             FXMLLoader loader =new FXMLLoader(getClass().getResource("ajoutartisan.fxml"));
        Parent root=loader.load();
         idret.getScene().setRoot(root);
    }

    @FXML
    private void afficheartisan(ActionEvent event) {
         try{
        myservicespi crud =new myservicespi();
        
        ObservableList<artisan> selectedRows , art ;
        art=tableart.getItems();
        selectedRows=tableart.getSelectionModel().getSelectedItems();
        FXMLLoader loader =new FXMLLoader(getClass().getResource("artisanprofile.fxml"));
        Parent root=loader.load();
        ArtisanprofileController aa=loader.getController();
        for(artisan a:selectedRows)
        {
            int id=a.getId();
          //  aa.setIdart(id);
            artisan detail=crud.detailartisan(id);
           // System.out.println(detail.getNumtel());
           aa.setNom(detail.getNomart());
            aa.setAdresse(detail.getAdresse());
            
//             bb.ssetImage(new Image(ClassLoader.getSystemResource("images/"+b1.getImage()).toString()));
           aa.setIdimage(new Image(ClassLoader.getSystemResource("images/"+a.getImage()).toString()));
            detail.getImage();
           aa.setTel(detail.getNumtel());
            
           
        }
        tableart.getScene().setRoot(root);
        }catch(IOException ex)
        {
            Logger.getLogger(BoutiqueprofileController.class.getName()).log(Level.SEVERE,null,ex);
        }
        
    }

    @FXML
    private void consume(KeyEvent event) {
         TextField txt_TextField = (TextField) event.getSource(); 
        int max_length = 8;
                
            if (txt_TextField.getText().length() >= max_length) {
                event.consume();
            }
            
            if(!event.getCharacter().matches("[0-9]")){
                event.consume();
            }

    }
    
}
