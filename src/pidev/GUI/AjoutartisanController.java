/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;
import javax.swing.JOptionPane;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import pidev.entities.artisan;
import pidev.services.UserService;
import pidev.services.myservicespi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class AjoutartisanController implements Initializable {

    @FXML
    private TextField nomart;
    @FXML
    private TextField addr;
    @FXML
    private TextField numtelf;
    @FXML
    private Label nomlabb;
    @FXML
    private Label adrlabb;
    @FXML
    private Label numtellabb;
    @FXML
    private Label imagelab;
    @FXML
    private Button btn1;
    @FXML
    private ImageView imageess;
private String fileName;
    @FXML
    private Button idlist;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        numtelf.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(oldValue){
                    if (numtelf.getText().length() == 8)
                        numtelf.setStyle("-fx-background-color: green");
                    else
                        numtelf.setStyle("-fx-background-color: red");
                }
            }
            
        });
        // TODO
    }    
    public  String upload(File file) throws  IOException {
        BufferedOutputStream stream = null;
        BufferedOutputStream stream2 = null;
        String globalPath="D:\\wamp6400\\www\\Pidevjava\\src\\images";
         String globalPath2="D:\\wamp6400\\www\\VERSION FINAAL\\PIDEV2\\web\\artisan_image";
       
        String localPath="localhost:8080/";
      fileName = file.getName();
        fileName=fileName.replace(" ", "_");
        try {
            Path p = file.toPath();
            
            byte[] bytes = Files.readAllBytes(p);
    
            File dir = new File(globalPath);
             File dir2 = new File(globalPath2);
            if (!dir.exists())
                dir.mkdirs();
            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()+File.separator + fileName);
                        File serverFile2 = new File(dir2.getAbsolutePath()+File.separator + fileName);

            stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream2 = new BufferedOutputStream(new FileOutputStream(serverFile2));
            stream.write(bytes);
            stream2.write(bytes);
            stream.close();
             stream2.close();
            return localPath+"/"+fileName;
        } catch (IOException ex) {
            Logger.getLogger(AjoutboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
            return "error2";
        
        }}

    @FXML
    private void chooseimagee(ActionEvent event) throws IOException  {
         FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("file", "*.jpg", "*.png"));
		File selectedfile = fileChooser.showOpenDialog(null);
		if (selectedfile != null) {
			
			upload(selectedfile);
                        System.out.println("    "+upload(selectedfile));
			Image image = new Image(new FileInputStream(selectedfile));
			imageess.setImage(image);
		}
    }

    @FXML
    private void ajouterArtisan(ActionEvent event) {
        artisan a;
        a = new artisan(addr.getText(),numtelf.getText(),nomart.getText(),fileName,LoginController.userc);
                myservicespi s=new myservicespi();
                  UserService u1=new UserService();
                  if(nomart.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ nom",""
                + "Erreur",2);  
         }else if(addr.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ dadresse",""
                + "Erreur",2);  
         }else if(numtelf.getText().equals("") && numtelf.getText().length() == 8)
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ numero de telephone",""
                + "Erreur",2);  
         }else if(fileName==null)
         
         {
           JOptionPane.showMessageDialog(null,"il faut choisissez une image",""
                + "Erreur",2);  
         }else
         {
                s.ajouterArtisan(a);
         }
    }

    @FXML
    private void listerart(ActionEvent event) throws IOException {
             FXMLLoader loader =new FXMLLoader(getClass().getResource("listeartisan.fxml"));
        Parent root=loader.load();
         idlist.getScene().setRoot(root);
    }

    @FXML
    private void consumer(KeyEvent event) {
        TextField txt_TextField = (TextField) event.getSource(); 
        int max_length = 8;
                
            if (txt_TextField.getText().length() >= max_length) {
                event.consume();
            }
            
            if(!event.getCharacter().matches("[0-9]")){
                event.consume();
            }

    }
    
}
