/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.PopOver;
import pidev.entities.Abonnement;
import pidev.entities.boutique;
import pidev.entities.notification;
import pidev.services.myservicespi;
import pidev.utils.myconnexionpi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class BoutiqueprofileController implements Initializable {

    public Button idab;
    @FXML
    public Button iddes;
    @FXML
    public Label nom;
    @FXML
    private Label adresse;
    @FXML
    private Label tel;

 public int idbt ;
    @FXML
    private Button idret;
    @FXML
    private ImageView idimage;
    @FXML
    private Button notifClick;
    @FXML
    private Label nbNotif;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
        
     
        
       myservicespi service=new myservicespi();
          List<notification> notifList = null ;
        try {
            notifList = service.listernotif(service.getUserBoutiquesIDs());
        } catch (SQLException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        nbNotif.setText(""+notifList.size());//        try {
//            Notify();
//        } catch (SQLException ex) {
//            Logger.getLogger(BoutiqueprofileController.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }    

    public ImageView getIdimage() {
        return idimage;
    }

    public void setIdimage(Image idimage) {
        this.idimage.setImage(idimage);
    }
   public void setNom(String nom) {
        this.nom.setText(nom);
    }

    public void setAdresse(String adresse) {
        this.adresse.setText(adresse);
    }

    public void setTel(String tel) {
        this.tel.setText(tel);
    }

    public void setIdbt(int idbt) {
        this.idbt = idbt;
    }
    

    private void abonnement(ActionEvent event) throws SQLException {
      //  Connection  cn = myconnexionpi.getInstance().getConnection();
        //ResultSet rs = cn.createStatement().executeQuery("SELECT * FROM `abonnement` WHERE id_user=" + iduser);
       
        Notifications.create()
                .title("abonnement Etablie ")
               .darkStyle()
               
               .hideAfter(Duration.seconds(20))
               .position(Pos.BOTTOM_LEFT)
               //
               .text(" NOm a fait un abonnement dans votre boutique")
               .showInformation();
   }

    @FXML
    private void desabonnement(ActionEvent event) throws SQLException{
         myservicespi crud =new myservicespi();
         Abonnement ab=new Abonnement();
         
         notification notif = new notification();
         java.sql.Date addedAt = new java.sql.Date(new java.util.Date().getTime());
        
        if(iddes.getText()=="abonnee")
        {
            System.out.println(idbt);
            ab.setId_boutique(idbt);
            ab.setId_user(LoginController.userc);
            
            notif.setAddedAt(addedAt);
            notif.setMessage(LoginController.username+" est abonné sur votre boutique");
            notif.setLink("link");
          //  notif.setIsRead(false);
            notif.setUser_id(LoginController.userc);
            notif.setIdboutique(idbt);
            System.out.println(addedAt);
            Notifications.create()
                .title("Abonnement de boutique ")
               .darkStyle()
               
               .hideAfter(Duration.seconds(20))
               .position(Pos.BOTTOM_LEFT)
               //
               .text(LoginController.username+ "  est abonné sur votre boutique")
               .showInformation();
            crud.ajouterAbonnement(ab);
            crud.ajouterNotification(notif);
            
            
            iddes.setText("desabonnee");
            
        }else{
            
            crud.supprimerAbonnement(LoginController.userc, idbt);
             iddes.setText("abonnee");
        }
    }
        
    
//   public void Notify() throws SQLException{
//        Connection  cn = myconnexionpi.getInstance().getConnection();
//        ResultSet rs = cn.createStatement().executeQuery("INSERT INTO notification (user_id,message)");
//      if (rs.next()){
//            int noti = rs.getInt(3);
//          System.out.println("aaaaaaaaaaa"+noti);
//      }
//        int noti = 0;
//      if (noti==0){
//      Notifications.create()
//               .title("abonnement Etablie ")
//               .darkStyle()
//               .hideAfter(Duration.seconds(20))
//               .position(Pos.BOTTOM_LEFT)
//               //
//               .text(" NOm a reservee un n nombre des places dans votre covoiturage")
//               .showInformation();}
//         String req4 ="UPDATE `abonnement` SET `notify` = 1 WHERE `abonnement`.`iduser` = " +LoginController.userc;
//         PreparedStatement statement2 = cn.prepareStatement(req4);
//         statement2.executeUpdate();
//      
//    } 

    @FXML
    private void retourenarriere(ActionEvent event) throws IOException {
        FXMLLoader loader =new FXMLLoader(getClass().getResource("Home.fxml"));
        Parent root=loader.load();
         idret.getScene().setRoot(root);
    }
    
//    @FXML
//    public void getNotification()
//    {
//        PopOver notif = new PopOver();
//        myservicespi service = new myservicespi();
//        
//        
//        notif.setArrowLocation(PopOver.ArrowLocation.TOP_RIGHT);
//        try {
//            
//        
//        List<notification> notifList = service.listernotif(service.getUserBoutiquesIDs()) ;
//        } catch (SQLException e) {
//        }
//        nbNotif.setText(""+notifList.size());
//        List<String> messageList=new ArrayList<String>();
//        for(notification n:notifList)
//        {
//            //tsajel les ids
//            messageList.add(n.getMessage());
//        }
//        ListView<String> viewList = new ListView<String>(); 
//        ObservableList<String> items =FXCollections.observableArrayList(messageList);
//        viewList.setItems(items);
//        viewList.setPrefSize(400, 200);
//        viewList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
//        @Override
//        public void changed(ObservableValue<? extends String>observable,String oldvalue,String newvalue)
//        {          
//            for (notification ntf : notifList) {
//            service.SetRead(ntf.getId());
//            }
//            
//        }
//        });
////          viewList.setOnMouseClicked(new EventHandler<MouseEvent>() {
////
////        @Override
////        public void handle(MouseEvent event) {
////            viewList.getItems().remove(event.getSource());
////        }});
//        //Label lb=new Label("tesst");
//        notif.setContentNode(viewList);
//        notif.setDetachable(false);
//      //  ntf.setHeight(200);
//           notif.show(notifClick);
//    }

 
    
   }


   
    

