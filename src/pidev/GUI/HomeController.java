/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import org.controlsfx.control.PopOver;
import pidev.entities.notification;
import pidev.services.myservicespi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class HomeController implements Initializable {

    @FXML
    private Button logid;
    @FXML
    private Button notifClick;
    @FXML
    private Label nbNotif;
    @FXML
    private ImageView notifClick1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        myservicespi service=new myservicespi();
          List<notification> notifList = null ;
        try {
            notifList = service.listernotif(service.getUserBoutiquesIDs());
        } catch (SQLException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        nbNotif.setText(""+notifList.size());
        // TODO
    }    

    @FXML
    private void logout(ActionEvent event) throws IOException {
         FXMLLoader loader =new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent root=loader.load();
         logid.getScene().setRoot(root);
    }
    
    
    @FXML
    public void getNotification() throws SQLException
    {
        PopOver notif = new PopOver();
        myservicespi service = new myservicespi();
        
        
        notif.setArrowLocation(PopOver.ArrowLocation.TOP_RIGHT);
        //nraj3 notif lel boutique conencté fi lista
        List<notification> notifList = service.listernotif(service.getUserBoutiquesIDs()) ;
        nbNotif.setText(""+notifList.size());
        List<String> messageList=new ArrayList<>();
        for(notification n:notifList)
        {
            //tsajel les ids
            messageList.add(n.getMessage());
        }
        ListView<String> viewList = new ListView<String>(); 
        ObservableList<String> items =FXCollections.observableArrayList(messageList);
        viewList.setItems(items);
        viewList.setPrefSize(400, 200);
        viewList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
        @Override
        public void changed(ObservableValue<? extends String>observable,String oldvalue,String newvalue)
        {          
            for (notification ntf : notifList) {
            service.SetRead(ntf.getId());
            }
            
        }
        });
//          viewList.setOnMouseClicked(new EventHandler<MouseEvent>() {
//
//        @Override
//        public void handle(MouseEvent event) {
//            viewList.getItems().remove(event.getSource());
//        }});
        //Label lb=new Label("tesst");
        notif.setContentNode(viewList);
        notif.setDetachable(false);
      //  ntf.setHeight(200);
           notif.show(notifClick1);
    }

    
}
