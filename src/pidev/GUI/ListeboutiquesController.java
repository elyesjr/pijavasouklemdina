/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import static pidev.GUI.LoginController.userc;
import pidev.entities.boutique;
import pidev.entities.user;
import pidev.services.myservicespi;
import pidev.utils.myconnexionpi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class ListeboutiquesController implements Initializable {

    private ImageView image;
    @FXML
    private TableColumn<boutique, String> idnombout1;
    @FXML
    private TableColumn<boutique, String> idadr1;
    @FXML
    private TableColumn<boutique, Integer> idnumtel;
    @FXML
    private TableColumn<boutique, String> idim;
    private ObservableList<boutique> data;
      private ObservableList<boutique> data1;
    
     static  int Id4MB;
    @FXML
    private TableView<boutique> table;
    @FXML
    private Button updatebtn;
    @FXML
    private Button deletebtn;
    @FXML
    private TextField txtnom;
    @FXML
    private TextField txtadr;
    @FXML
    private TextField txtnumtel;
    @FXML
    private Button btnimg;
    private TextField imidg;
   
     String fileName;
    private TextField imagid;
    @FXML
    private ImageView images;
    @FXML
    private TextField rechercher;
    @FXML
    private Button idrech;
    @FXML
    private Button ifaffiche;
    @FXML
    private Button idretourretourner;
    @FXML
    private Button listbid;
    @FXML
    private ImageView iddel;
    @FXML
    private ImageView idupd;
  
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        try {
//            listebout();
//            
//           
//        //    recherche();
//        } catch (SQLException ex) {
//            Logger.getLogger(ListeboutiquesController.class.getName()).log(Level.SEVERE, null, ex);
//        }
txtnumtel.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(oldValue){
                    if (txtnumtel.getText().length() == 8)
                        txtnumtel.setStyle("-fx-background-color: green");
                    else
                        txtnumtel.setStyle("-fx-background-color: red");
                }
            }   });
        boutique b=new boutique();
        setCellValueFromTableToTextFieled();
      
         
}   


    public void listebout() throws SQLException {
   Connection cn = myconnexionpi.getInstance().getConnection();
   myservicespi croud=new myservicespi();
   
     List<boutique> lb=croud.listerboutiques();
        ObservableList<boutique> data = FXCollections.observableArrayList();
      
         //     ResultSet rs = cn.createStatement().executeQuery("SELECT * FROM `boutique`");
    for(boutique e:lb)
    {
        data.add(e);
                 idadr1.setCellValueFactory(new PropertyValueFactory<>("adresse"));

        idnombout1.setCellValueFactory(new PropertyValueFactory<>("nombout"));
            idim.setCellValueFactory(new PropertyValueFactory<>("image"));
        idnumtel.setCellValueFactory(new PropertyValueFactory<>("numtel"));
     
//    InputStream is=lb.getBinaryStream(image1);
    
    
//        OutputStream os =new FileOutputStream(new File(file));
            
          table.setItems(null);  
        table.setItems(data);   
       
    }        
    }
// public void affichermod(ActionEvent event) throws IOException {
//        ObservableList<boutique> selectedRows, all;
//        all = table.getItems();
//
//        selectedRows = table.getSelectionModel().getSelectedItems();
//
//        for (boutique b : selectedRows) {
//            try {
//
//                int id = b.getId();
//                System.out.println("" + id);
//                System.out.println("desole");
//                FXMLLoader fxmlLoader = new FXMLLoader();
//                fxmlLoader.setLocation(getClass().getResource("modifierbout.fxml"));
//                /* 
//         * if "fx:controller" is not set in fxml
//         * fxmlLoader.setController(NewWindowController);
//                 */
//                Scene scene = new Scene(fxmlLoader.load());
//                Stage stage = new Stage();
//
//                stage.setScene(scene);
//                stage.show();
//                // Hide this current window (if this is what you want)
//            } catch (NumberFormatException e) {
//                JOptionPane.showMessageDialog(null, "Id not figured");
//
//            }
//
//        }
//    }

    @FXML
    public void update(ActionEvent event) throws SQLException, IOException  {
          
       Connection cn= myconnexionpi.getInstance().getConnection();
       myservicespi crud=new myservicespi();
        
    ObservableList<boutique> selectedRows, bout;
        bout= table.getItems();
        selectedRows = table.getSelectionModel().getSelectedItems();
           
           
        for(boutique b:selectedRows)
        {
        boutique bb=new boutique();
           System.out.println(b.getId());
            int id=b.getId();
            b.setNombout(txtnom.getText());
            b.setNumtel(txtnumtel.getText());
            b.setAdresse(txtadr.getText());
            if (fileName==null)
                
            b.setImage(b.getImage());
            else 
                b.setImage(fileName);
            listebout();
           File file = null;
            upload(file);
            crud.ModifierBoutique(b, id);
      //  modif.setId1(id);
       //    modif.b.setId(b.getId());
          
           
    
        }
     
           
        
        //}
//        boutique b =new boutique(txtnumtel.getText(),txtadr.getText(),txtnom.getText(),fileName);
//        myservicespi sp=new myservicespi();
//        setCellValueFromTableToTextFieled();
//   sp.ajouterBoutique(b);
        
}

    @FXML
    private void delete(ActionEvent event) {
         Connection cn= myconnexionpi.getInstance().getConnection();
       myservicespi crud=new myservicespi();
        
    ObservableList<boutique> selectedRows, bout;
        bout= table.getItems();
        selectedRows = table.getSelectionModel().getSelectedItems();
           
           
        for(boutique b:selectedRows)
        {
        boutique bb=new boutique();
           System.out.println(b.getId());
            int id=b.getId();
        
            crud.supprimerBoutique(id);
        
        }
       
    }
 public  String upload(File file) throws  IOException {
          return "error2";
        
        }
    @FXML
    private void chooseimage(ActionEvent event) throws IOException {
         FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("file", "*.jpg", "*.png"));
		File selectedfile = fileChooser.showOpenDialog(null);
		if (selectedfile != null) {
			
			upload(selectedfile);
                        System.out.println("    "+upload(selectedfile));
			Image image1 = new Image(new FileInputStream(selectedfile));
			images.setImage(image1);
		}
    }
    private void setCellValueFromTableToTextFieled(){
        table.setOnMouseClicked(e -> {
            
            boutique b1=table.getItems().get(table.getSelectionModel().getSelectedIndex());
            txtnom.setText(b1.getNombout());
            txtadr.setText(b1.getAdresse());
            txtnumtel.setText(b1.getNumtel());
            
            images.setImage(new Image(ClassLoader.getSystemResource("images/"+b1.getImage()).toString()));
              System.out.print(b1.getUserid());
             if (LoginController.userc==b1.getUserid())
                {
                
        updatebtn.setVisible(true);
        deletebtn.setVisible(true);
        iddel.setVisible(true);
        idupd.setVisible(true);
                }  else{ updatebtn.setVisible(false);
               deletebtn.setVisible(false);
               iddel.setVisible(false);
        idupd.setVisible(false);
               
             }
        });
}
    
//       public void recherche() {
//
//
//        rechercher.setOnKeyReleased(e -> {
//             
//            if (rechercher.getText().equals("")) {
//               
//               System.out.print("erruer");
//                
//            } else { data.clear();
//                String sql = "SELECT * FROM `boutique` WHERE numero LIKE '%" + rechercher.getText() + "%' "
//                        + "UNION SELECT * FROM `boutique` WHERE depart LIKE '%" + rechercher.getText() + "%'"
//                        + "UNION SELECT * FROM `boutique` WHERE arriver LIKE '%" + rechercher.getText() + "%'"
//                        + "UNION SELECT * FROM `boutique` WHERE horraire LIKE '%" + rechercher.getText() + "%'";
//                Connection cn = myconnexionpi.getInstance().getConnection();
//                try {
//                    
//                      PreparedStatement statemnt =cn.prepareStatement(sql);
//                  
//                  
//                    ResultSet rs = statemnt.executeQuery();
//                    while (rs.next()) {
//                        data.add(new boutique(rs.getString(3), rs.getString(4), rs.getString(6), rs.getString(5)));
//                    }
//                    table.setItems(data);
//                } catch (SQLException ex) {
//                    Logger.getLogger(ListeboutiquesController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//               
//            }
//        });
//      }

    @FXML
    public void recherche(ActionEvent event) {
          boutique b =new boutique();
        myservicespi sp=new myservicespi();
//   sp.rechercherBoutique(idrech.getText());}

        String search = rechercher.getText();
        
        table.setItems(sp.rechercherBoutique(search));
        
    }

    @FXML
    private void afficheboutique(ActionEvent event) throws SQLException {
        try{
        myservicespi crud =new myservicespi();
        
        ObservableList<boutique> selectedRows , bout ;
        bout=table.getItems();
        selectedRows=table.getSelectionModel().getSelectedItems();
        FXMLLoader loader =new FXMLLoader(getClass().getResource("Boutiqueprofile.fxml"));
        Parent root=loader.load();
        BoutiqueprofileController bb=loader.getController();
        for(boutique b:selectedRows)
        {
            int id=b.getId();
            bb.setIdbt(id);
            boutique detail=crud.detailBoutique(id);
           // System.out.println(detail.getNumtel());
           bb.setNom(detail.getNombout());
            bb.setAdresse(detail.getAdresse());
//             bb.ssetImage(new Image(ClassLoader.getSystemResource("images/"+b1.getImage()).toString()));
           bb.setIdimage(new Image(ClassLoader.getSystemResource("images/"+b.getImage()).toString()));
            detail.getImage();
            bb.setTel(b.getNumtel());
            
            if(crud.isAbonne(userc, id)==true)
            {
              bb.iddes.setText("desabonnee");
          
            }else {
              bb.iddes.setText("abonnee");
            }
        }
        table.getScene().setRoot(root);
        }catch(IOException ex)
        {
            Logger.getLogger(BoutiqueprofileController.class.getName()).log(Level.SEVERE,null,ex);
        }
        
        
    }

    @FXML
    private void retourner(ActionEvent event) throws IOException {
             FXMLLoader loader =new FXMLLoader(getClass().getResource("Ajoutboutique.fxml"));
        Parent root=loader.load();
         idretourretourner.getScene().setRoot(root);
    }

    @FXML
    private void listeb(ActionEvent event) {
          try {
            listebout();
            
           
        //    recherche();
        } catch (SQLException ex) {
            Logger.getLogger(ListeboutiquesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void consume(KeyEvent event) {
        TextField txt_TextField = (TextField) event.getSource(); 
        int max_length = 8;
                
            if (txt_TextField.getText().length() >= max_length) {
                event.consume();
            }
            
            if(!event.getCharacter().matches("[0-9]")){
                event.consume();
            }
    }


}
    
    



    
    

