/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.controlsfx.control.Notifications;
import pidev.entities.boutique;
import pidev.entities.user;
import pidev.services.UserService;
import pidev.services.myservicespi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class AjoutboutiqueController implements Initializable {

    @FXML
    private Button btn;
    @FXML
    private TextField nombout;
    @FXML
    private TextField add;
    @FXML
    private TextField numtel;
    @FXML
    private ImageView im;
    @FXML
    private Label nomlab;
    @FXML
    private Label adrlab;
    @FXML
    private Label numlab;
    @FXML
    private Label imlab;
    @FXML
    private Button upload;
    String fileName ;
    @FXML
    private Button notif;
    @FXML
    private Label labnotif;
    @FXML
    private Button idliste;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        numtel.focusedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(oldValue){
                    if (numtel.getText().length() == 8)
                        numtel.setStyle("-fx-background-color: green");
                    else
                        numtel.setStyle("-fx-background-color: red");
                }
            }
            
        });
}  

    
    
    
//    FXMLLoader loader=new FXMLLoader(getClass().getResource("AffichageFXML.fxml"));
//    Parent root;
//        try {
//            root = loader.load();
//             AffichageFXMLController personneConroller=loader.getController();
//    personneConroller.setNomaff(Nom.getText());
//     personneConroller.setPrenomaff(Prenom.getText());
//     Nom.getScene().setRoot(root);
//    
//            
//        } catch (IOException ex) {
//            Logger.getLogger(WorkshopjavaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    

    
    
    
      public  String upload(File file) throws  IOException {
        BufferedOutputStream stream = null;
        BufferedOutputStream stream2 = null;
        String globalPath="D:\\wamp6400\\www\\Pidevjava\\src\\images";
         String globalPath2="D:\\wamp6400\\www\\VERSION FINAAL\\PIDEV2\\web\\boutique_image";
        String localPath="localhost:8080/";
       fileName = file.getName();
        fileName=fileName.replace(" ", "_");
        try {
            Path p = file.toPath();
            
            byte[] bytes = Files.readAllBytes(p);
    
            File dir = new File(globalPath);
             File dir2 = new File(globalPath2);
            if (!dir.exists())
                dir.mkdirs();
            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()+File.separator + fileName);
            File serverFile2 = new File(dir2.getAbsolutePath()+File.separator + fileName);
          
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));
             stream2 = new BufferedOutputStream(new FileOutputStream(serverFile2));
            stream.write(bytes);
            stream2.write(bytes);
            stream.close();
             stream2.close();
            return localPath+"/"+fileName;
        } catch (IOException ex) {
            Logger.getLogger(AjoutboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
            return "error2";
        
        }}
    @FXML
    private void chooseimage(ActionEvent event) throws IOException {
        
        FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("file", "*.jpg", "*.png"));
		File selectedfile = fileChooser.showOpenDialog(null);
		if (selectedfile != null) {
			
			upload(selectedfile);
                        System.out.println("    "+upload(selectedfile));
			Image image = new Image(new FileInputStream(selectedfile));
			im.setImage(image);
		}
             //   this.Mypath = txtpath.getText();
//    FileChooser fileChooser = new FileChooser();
//             
//            //Set extension filter
//            FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
//            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
//            fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
//              
//            //Show open file dialog
//            File file = fileChooser.showOpenDialog(null);
//                       
//            try {
//                BufferedImage bufferedImage = ImageIO.read(file);
//                Image image1 = SwingFXUtils.toFXImage(bufferedImage, null);
//                im.setImage(image1);
//            } catch (IOException ex) {
//                Logger.getLogger(AjoutboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
//            }
 }
    @FXML
    private void ajouterBoutique(ActionEvent event) {
       
        boutique b =new boutique(add.getText(),numtel.getText(),nombout.getText(),fileName,LoginController.userc);
        myservicespi sp=new myservicespi();
        UserService u1=new UserService();
      //  b.setUserid(LoginController.userc);
      //  System.out.println(LoginController.userc);
      
       
//       notif.getNodeOrientation().notify();
        
        if(nombout.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ nom",""
                + "Erreur",2);  
         }else if(add.getText().equals(""))
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ dadresse",""
                + "Erreur",2);  
         }else if(numtel.getText().equals("") && numtel.getText().length() == 8)
         
         {
           JOptionPane.showMessageDialog(null,"il faut remplir le champ numero de telephone",""
                + "Erreur",2);  
         }else if(fileName==null)
         
         {
           JOptionPane.showMessageDialog(null,"il faut choisissez une image",""
                + "Erreur",2);  
         }else
         {
             
            sp.ajouterBoutique(b);
            Notifications.create()
                .title("boutique créer ")
               .darkStyle()
               
               .hideAfter(Duration.seconds(20))
               .position(Pos.BOTTOM_LEFT)
               //
               .text(b.getNombout()+ " est un nouveau boutique qui a été ajouté dans votre site")
               .showInformation();
    }}
         

    @FXML
    private void listenotif(ActionEvent event) {
        
    }

    @FXML
    private void listerboutique(ActionEvent event) throws IOException {
        
           FXMLLoader loader =new FXMLLoader(getClass().getResource("listeboutiques.fxml"));
        Parent root=loader.load();
         idliste.getScene().setRoot(root);
    }

    @FXML
    private void consumer(KeyEvent event) {
        TextField txt_TextField = (TextField) event.getSource(); 
        int max_length = 8;
                
            if (txt_TextField.getText().length() >= max_length) {
                event.consume();
            }
            
            if(!event.getCharacter().matches("[0-9]")){
                event.consume();
            }
    }
    
    
    
}


