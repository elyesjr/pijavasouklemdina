/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.GUI;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pidev.utils.myconnexionpi;

/**
 * FXML Controller class
 *
 * @author MBM info
 */
public class PiechartController implements Initializable {

    @FXML
    private PieChart piechart;
    @FXML
    private Button btnchart;
    @FXML
    private Label caption = new Label("");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btn(ActionEvent event) throws SQLException {
        //boutique b=new boutique();       
        
        // Create a connection to the DB
        //ObservableList<Object> data1 = FXCollections.observableArrayList();
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        String SQL_1 = "SELECT SUM(quantité) as somme FROM produit ";
        String SQL_2 = "SELECT titre, quantité FROM produit ";
        Connection cn = null;
        Statement statement = null;
        ResultSet resultat = null;
        int sum = 0;
        //Label
       
        caption.setTextFill(Color.BLACK);
        caption.setStyle("-fx-font: 16 arial;");
        
        try{
        cn = myconnexionpi.getInstance().getConnection();
        statement = cn.createStatement();
        resultat = statement.executeQuery(SQL_1);
           
        while(resultat.next()){
            String sommeQ = resultat.getString("somme");
            //sum = Integer.parseInt("sommeQ");
            sum = Integer.parseInt(sommeQ);
            
        }
        
        resultat = statement.executeQuery(SQL_2);
        while(resultat.next()){
            String titre = resultat.getString("titre");
            double quantite = resultat.getInt("quantité");
            double pourcentage = Double.parseDouble(String.format("%.2f",(quantite/sum)*100).replace(',', '.')) ;
            
            pieChartData.add(new PieChart.Data(titre, pourcentage));
            
        }
        
        piechart.setData(pieChartData);
        
        pieChartData.forEach((data) ->{
            data.getNode().addEventHandler(MouseEvent.MOUSE_MOVED,(MouseEvent e) -> {
                caption.setTranslateX(e.getSceneX());
                caption.setTranslateY(e.getSceneY());
                caption.setText(String.valueOf(data.getPieValue()) + "%");
                caption.setVisible(true);
            });
            data.getNode().addEventHandler(MouseEvent.MOUSE_EXITED,(MouseEvent e) -> {
                caption.setVisible(false);
            });
            });
        
            }catch (SQLException e){}
            finally {
        if ( resultat != null ) {
            try {
                /* On commence par fermer le ResultSet */
                resultat.close();
            } catch ( SQLException ignore ) {
            }
        }
        if ( statement != null ) {
            try {
                /* Puis on ferme le Statement */
                statement.close();
            } catch ( SQLException ignore ) {
            }
        }
        if ( cn != null ) {
            try {
                /* Et enfin on ferme la connexion */
                cn.close();
            } catch ( SQLException ignore ) {
            }
        } 
        
        
        

//        // read all elements from tble : requete sql read et tu vas mettre dans une structure
//        ArrayListData from database
//        //machine learning
//        // Create 
//                ObservableList<PieChart.Data> pieChartData = null;
//                
//        for (parcourir la ArrayListData)
//            var month = ArrayListData(i).month
//            var number = ArrayListData(i).number
//            pieChartData.addElementToArrayList(new PieChart.Data(month, number))
        
        //String req1 = "count * from boutique nom_bout ";
        
                
    }
    
    }
}
